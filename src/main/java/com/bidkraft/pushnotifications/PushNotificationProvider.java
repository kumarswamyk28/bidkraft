package com.bidkraft.pushnotifications;

public interface PushNotificationProvider {
	
	public boolean push(String deviceToken, String title, String body);

}
