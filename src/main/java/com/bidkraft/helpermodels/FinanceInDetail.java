package com.bidkraft.helpermodels;
/***
 * 
 * @author mgunnam
 * This is the model created to basically set with all the finance detail after calculation on Bid placing by the
 * provider 
 */
public class FinanceInDetail {
	
	private double bidAmountQuoted;
	private double feeAmount;
	private double totalAmountToGetFromServiceSeeker;
	
	
	public double getBidAmountQuoted() {
		return bidAmountQuoted;
	}
	public void setBidAmountQuoted(double bidAmountQuoted) {
		this.bidAmountQuoted = bidAmountQuoted;
	}
	public double getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}
	public double getTotalAmountToGetFromServiceSeeker() {
		return totalAmountToGetFromServiceSeeker;
	}
	public void setTotalAmountToGetFromServiceSeeker(double totalAmountToGetFromServiceSeeker) {
		this.totalAmountToGetFromServiceSeeker = totalAmountToGetFromServiceSeeker;
	}


	
	
	
	
}
