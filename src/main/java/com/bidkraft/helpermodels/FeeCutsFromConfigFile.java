package com.bidkraft.helpermodels;

public class FeeCutsFromConfigFile {

	private double bidkraftsPerCut;
	private double luisPerCut;
	private double paypalPaymentPerCut;
	private double paypalPaymenFlatCut;
	private double paypalPayoutFlatCut;
	
	
	public double getBidkraftsPerCut() {
		return bidkraftsPerCut;
	}
	public void setBidkraftsPerCut(double bidkraftsPerCut) {
		this.bidkraftsPerCut = bidkraftsPerCut;
	}
	
	public double getLuisPerCut() {
		return luisPerCut;
	}
	public void setLuisPerCut(double luisPerCut) {
		this.luisPerCut = luisPerCut;
	}
	
	
	public double getPaypalPaymentPerCut() {
		return paypalPaymentPerCut;
	}
	public void setPaypalPaymentPerCut(double paypalPaymentPerCut) {
		this.paypalPaymentPerCut = paypalPaymentPerCut;
	}
	
	
	public double getPaypalPaymenFlatCut() {
		return paypalPaymenFlatCut;
	}
	public void setPaypalPaymenFlatCut(double paypalPaymenFlatCut) {
		this.paypalPaymenFlatCut = paypalPaymenFlatCut;
	}
	
	
	public double getPaypalPayoutFlatCut() {
		return paypalPayoutFlatCut;
	}
	public void setPaypalPayoutFlatCut(double paypalPayoutFlatCut) {
		this.paypalPayoutFlatCut = paypalPayoutFlatCut;
	}
	
	
	
	
	
}
