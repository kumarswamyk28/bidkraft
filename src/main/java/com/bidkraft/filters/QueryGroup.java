package com.bidkraft.filters;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

public class QueryGroup implements Query {
	
	@JsonProperty("operator")
	private String operator;
	
	@JsonProperty("queries")
	private List<Query> queries;
	
	public QueryGroup() {
		operator = "and";
		queries = new ArrayList<Query>();
	}
	
	public QueryGroup(String operator, List<Query> queries){
		this.operator = operator;
		this.queries = queries;
	}
	
	public void addQuery(Query query) {
		queries.add(query);
	}

	@Override
	public QueryBuilder getQuery() {
		BoolQueryBuilder result = QueryBuilders.boolQuery();
		if(queries.size() > 0){
			for(Query q : queries) {
				
				if(operator.equals("and")){
					result.must(q.getQuery());
				} else
					result.mustNot(q.getQuery());
			}
		}
		return result;
	}

}
