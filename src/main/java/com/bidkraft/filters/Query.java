package com.bidkraft.filters;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonSubTypes.Type;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.elasticsearch.index.query.QueryBuilder;

@JsonTypeInfo(  
	    use = JsonTypeInfo.Id.NAME,  
	    include = JsonTypeInfo.As.PROPERTY,  
	    property = "type")  
	@JsonSubTypes({  
		@Type(value = QueryGroup.class, name = "group"),
	    @Type(value = GeoQuery.class, name = "geo"),  
	    @Type(value = TextQuery.class, name = "text") })  
public interface Query {

	public QueryBuilder getQuery();
		
}
