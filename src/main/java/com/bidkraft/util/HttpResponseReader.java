package com.bidkraft.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.springframework.stereotype.Component;

@Component
public class HttpResponseReader {

		public String readHttpResponseContent(HttpResponse response) throws IOException{
			BufferedReader rd = new BufferedReader(
	                new InputStreamReader(response.getEntity().getContent()));
			
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			return result.toString();
		}
	
}
