package com.bidkraft.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.bidkraft.helpermodels.FeeCutsFromConfigFile;

public class ConfigFileReader {

	// this following method for reading the individual properties written in config file
	
	public FeeCutsFromConfigFile getFeeCutRealtedObjFromConfig() throws IOException{
		
		FeeCutsFromConfigFile feeCutsObj = new FeeCutsFromConfigFile();
				
			Properties prop = new Properties();
			InputStream ipStream = null;
			try {
				String propFileName = "config.properties";
				ipStream = getClass().getClassLoader().getResourceAsStream(propFileName);
				if (ipStream != null) {
					prop.load(ipStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
				feeCutsObj.setBidkraftsPerCut(Double.parseDouble(prop.getProperty("bidkraftpercentcut")));
				feeCutsObj.setLuisPerCut(Double.parseDouble(prop.getProperty("luispercentcut")));
				feeCutsObj.setPaypalPaymenFlatCut(Double.parseDouble(prop.getProperty("paypalpaymentflatcut")));
				feeCutsObj.setPaypalPaymentPerCut(Double.parseDouble(prop.getProperty("paypalpaymentpercentcut")));
				feeCutsObj.setPaypalPayoutFlatCut(Double.parseDouble(prop.getProperty("paypalpayoutflatcut")));
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			} finally {
				ipStream.close();
			}
			return feeCutsObj;
	}
	
	

	
	//************* this following methods for reading the individual properties written in config file
	
	public double getBidKraftCut() throws IOException{
		
		double bidKraftCut = 0.0;
		Properties prop = new Properties();
		InputStream ipStream = null;
		try {
			
			String propFileName = "config.properties";
 
			
			ipStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (ipStream != null) {
				prop.load(ipStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			bidKraftCut = Double.parseDouble(prop.getProperty("bidkraftpercentcut"));
			
			
			
			System.out.println("BidKraft Cut: "+ bidKraftCut);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			ipStream.close();
		}
		return bidKraftCut;
	}
	
	
	public double getluisCut() throws IOException{			
			double luisCut = 0.0;
			Properties prop = new Properties();
			InputStream ipStream = null;
			try {
				
				String propFileName = "config.properties";
				ipStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 
				if (ipStream != null) {
					prop.load(ipStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
	
				luisCut = Double.parseDouble(prop.getProperty("luispercentcut"));
				System.out.println("luis Cut: "+ luisCut);
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			} finally {
				ipStream.close();
			}
			return luisCut;
		}
		
		
	
	
}
