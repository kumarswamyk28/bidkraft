package com.bidkraft.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class JsonMapper {
	private static ObjectMapper mapper;

	public ObjectMapper getObjectMapper() {
		if (mapper != null) {
			return mapper;
		} else {
			mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
			mapper.setDateFormat(df);
			return mapper;
		}

	}

}
