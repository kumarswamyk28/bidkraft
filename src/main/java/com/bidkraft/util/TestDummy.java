package com.bidkraft.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.User;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.paypalutil.PaypalPaymentUtil;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.HttpResponseReader;
import com.paypal.api.payments.PayoutBatch;

@Component
@SuppressWarnings("unchecked")
public class TestDummy implements KraftService<User>{

	@Override
	public User service(KraftRequest request) throws KraftException {
		User userObj = new User();
		if (request.getEntities().containsKey(ServiceKeys.TESTDUMMY)) {
			Map<String, String> getUserDetailAuthMap = (Map<String, String>) request.getEntities().get(ServiceKeys.TESTDUMMY);
			System.out.println("This is the service used for generalised testing by harcoding the values and stuff:");			
			
			try{
						
				String accessKeyfromWebAPI = getInitialAuthCodeFromWebAPI();
				System.out.println(accessKeyfromWebAPI);
				
				
				// the following object would have structure of something like the below 
				/*	{
					  "user_id": "https://www.paypal.com/webapps/auth/server/64ghr894040044",
					  "name": "Peter Pepper",
					  "given_name": "Peter",
					  "family_name": "Pepper",
					  "email": "ppuser@example.com"
					}*/
				
				// this following lines are just for the Testing Purpose
				HttpResponse response =  getIdentityByAccessToken(accessKeyfromWebAPI);
				HttpResponseReader httpResponseReader = new HttpResponseReader();
				String httpRespStr =  httpResponseReader.readHttpResponseContent(response);

				// commented as the following string kind of replica is now created from the above service call to server
				//String httpRespStr = "{'address':{'postal_code':'95131','locality':'San Jose','region':'CA','country':'US','street_address':'3 Main St'},'family_name':'Smith','language':'en_US','phone_number':'4082560980','locale':'en_US','name':'Roger Smith','email':'rsmith@somewhere.com','account_type':'PERSONAL','birthday':'1982-08-02','given_name':'Roger','user_id':'https://www.paypal.com/webapps/auth/identity/user/jG8zVpn2toXCPmzNffW1WTRLA2KOhPXYybeTM9p3ct0'}";
				JSONObject paypalIdentityJObj = new JSONObject(httpRespStr);
				userObj = addUserIfNotExisting(paypalIdentityJObj);

			}catch (Exception e) {
			
				// TODO: handle exception
				KraftException ke = new KraftException(e.getMessage());
				throw ke;
			}
		
		}
		
		return userObj;
	}
	
	
	
	// this method is to get the Paypal User Identity from the accessToken
		@SuppressWarnings("deprecation")
		public HttpResponse getIdentityByAccessToken(String accessToken) throws ClientProtocolException, IOException{	
			
			// this is the url for the Identity openid connect
			String url = "https://api.sandbox.paypal.com/v1/identity/openidconnect/userinfo/?schema=openid";
			@SuppressWarnings("resource")
			HttpClient client = new DefaultHttpClient();
			HttpGet getReq = new HttpGet(url);	
			
			// add header	
			getReq.setHeader("Content-Type", "application/json");
			getReq.setHeader("Authorization", "Bearer "+accessToken);
			HttpResponse response = client.execute(getReq);

			System.out.println("\n*** Getting the User Identity **** Sending 'Get' request to URL : " + url);
			System.out.println("Response Code : " +
	                                    response.getStatusLine().getStatusCode());
			return response;
		}
		
	
	
	
	
	public String getInitialAuthCodeFromWebAPI() throws ClientProtocolException, IOException, JSONException{
		String url = "https://api.sandbox.paypal.com/v1/oauth2/token";
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		String authStrInHeader = PaymentPaypalKeys.merchantId+":"+PaymentPaypalKeys.merchantSecret;
		//String authStrInHeader = "mgunn001-buyer1@odu.edu"+":"+"12345678";

		String base64AuthStrInHeader = Base64.getUrlEncoder().encodeToString(authStrInHeader.getBytes("utf-8"));
		
		// add header	
		post.setHeader("Content-Type", "application/x-www-form-urlencoded");	
		post.setHeader("Authorization", "BASIC "+base64AuthStrInHeader);
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));


		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);
		
		
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

		HttpResponseReader httpResponseReader = new HttpResponseReader();
		String httpRespStr =  httpResponseReader.readHttpResponseContent(response);	
		JSONObject jobj = new JSONObject(httpRespStr);
		return jobj.getString("access_token");

	}
	
	
	@SuppressWarnings("unchecked")
	public User addUserIfNotExisting(JSONObject paypalIdentityJObj) throws KraftException{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transcation = session.beginTransaction();
		User userObj = new User();
		try{
			
			// these two are changed cause a paypal user now is uniquely identified by user_id
			//String emailIdFromIdentity = paypalIdentityJObj.getString("email");
			//Query getUserIfExistQuery = session.createQuery("from User where email = :emailId");

			
			String user_idFromIdentity = paypalIdentityJObj.getString("user_id");
			Query getUserIfExistQuery = session.createQuery("from User where paypalUserURL = :userURL");
			getUserIfExistQuery.setParameter("userURL", user_idFromIdentity);
			
			List<User> usersList = getUserIfExistQuery.list();
	
			if(usersList.size() > 0) {				
				userObj = usersList.get(0);								
			}else{
				// if none exist in the DB with this emailId, Create an user with these detail, may also have to capture the Phone number etc
				
				if(paypalIdentityJObj.has("address") && paypalIdentityJObj.has("email")){
					
					JSONObject latLongObj = getLatLongFromAddress(paypalIdentityJObj.getJSONObject("address"));
					double lat = (double)((JSONObject)latLongObj.getJSONArray("results").get(0)).getJSONObject("geometry").getJSONObject("location").get("lat");
					double lng = (double)((JSONObject)latLongObj.getJSONArray("results").get(0)).getJSONObject("geometry").getJSONObject("location").get("lng");
					userObj.setEmail(paypalIdentityJObj.getString("email"));
					userObj.setPaypalRegisteredEmail(paypalIdentityJObj.getString("email"));
					userObj.setPaypalUserURL(user_idFromIdentity);
					userObj.setPassword("dummydummy");		
					//these two have to be set by some other external API, for now setting it to the following hardcoded values	
					userObj.setLatitude(lat);
					userObj.setLongitude(lng);				
					userObj.setBgcstatus(ServiceKeys.BGC_INTIAL);
					userObj.setRating(Double.parseDouble("5"));		
					userObj.setIsPayPalAuth("no");
					
					String httpbolbStr = "{'address':{'postal_code':'95131','locality':'San Jose','region':'CA','country':'US','street_address':'3 Main St'},'family_name':'Smith','language':'en_US','phone_number':'4082560980','locale':'en_US','name':'Roger Smith','email':'rsmith@somewhere.com','account_type':'PERSONAL','birthday':'1982-08-02','given_name':'Roger','user_id':'https://www.paypal.com/webapps/auth/identity/user/jG8zVpn2toXCPmzNffW1WTRLA2KOhPXYybeTM9p3ct0'}";

					userObj.setPaypaIdentityResponse(Hibernate.createBlob(httpbolbStr.getBytes()));
					session.save(userObj);
					transcation.commit();
				}else{
					KraftException ke = new KraftException("User hasn't given Consent to access his Information.");
					throw ke;
				}
				
			}

		}catch(Exception e) {
			session.getTransaction().rollback();
			KraftException ke = new KraftException(e.getMessage());
			throw ke;
		}
		session.flush();
		return userObj;
	}

	
	// this method is to get the Latitude and Longitude from the Address string
	@SuppressWarnings("deprecation")
	public JSONObject getLatLongFromAddress(JSONObject address) throws JSONException, ClientProtocolException, IOException{	
		
		// this one is got from my EmailID : maheedhargunnam@gmail.com
		String APIKey = "AIzaSyA28n5arlMhJ8xjhVM7ReNQZcyXgP7IepU";
		
		//sample - http://maps.googleapis.com/maps/api/geocode/json?address=1049+W+49th%20street,+Norfolk,+Virginia&key=YOUR_API_KEY
		// this is the url for the Identity openid connect
		String url = "https://maps.googleapis.com/maps/api/geocode/json?address=";
			url+= URLEncoder.encode(address.getString("street_address")+", ","UTF-8");
			url+= URLEncoder.encode(address.getString("locality")+", ","UTF-8");
			url+= URLEncoder.encode(address.getString("region")+", ","UTF-8");
			url+= URLEncoder.encode(address.getString("postal_code"),"UTF-8");
			url+="&key="+APIKey;
		

		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpGet getReq = new HttpGet(url);	
		
		HttpResponse response = client.execute(getReq);

		System.out.println("\n*** Getting the Geo Location (Lat, Long) From Address **** Sending 'Get' request to URL : " + url);
		System.out.println("Response Code : " +response.getStatusLine().getStatusCode());
		HttpResponseReader httpResponseReader = new HttpResponseReader();
		String httpRespStr =  httpResponseReader.readHttpResponseContent(response);
		JSONObject latLongObj = new JSONObject(httpRespStr);
		return latLongObj;
	}
	

}
