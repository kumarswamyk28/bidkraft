package com.bidkraft.services.userrequests;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
//import com.bidkraft.elasticsearch.ElasticsearchAPIFactory;
import com.bidkraft.elasticsearch.FinderAPI;
import com.bidkraft.entities.Request;
import com.bidkraft.exception.KraftException;
import com.bidkraft.filters.TextQuery;

import org.hibernate.Query;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.responses.UserBid;
import com.bidkraft.responses.UserRequestResponse;
import com.bidkraft.services.KraftService;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * TotalBids, BestBid, Title, Description, Request-StartDate
 * @author Abhishek
 *
 */

@Component
public class GetAvailableRequestsService implements KraftService<List<UserRequestResponse>> {

	@Override
	public List<UserRequestResponse> service(KraftRequest request) throws KraftException {

		List<UserRequestResponse> requests = new ArrayList<UserRequestResponse>();
		try {
			if (request.getEntities().containsKey(ServiceKeys.GETAVAILABLEREQUESTS)) {
				@SuppressWarnings("unchecked")
				Map<String, Object> requestMap = (Map<String, Object>) request.getEntities()
						.get(ServiceKeys.GETAVAILABLEREQUESTS);

				//FinderAPI finderAPI = ElasticsearchAPIFactory.getFinderAPI();
				//@SuppressWarnings("unchecked")
				
				@SuppressWarnings("unchecked")
				Map<String, Object> filters = (Map<String, Object>) requestMap.get("filter");
				@SuppressWarnings("unchecked")
				ArrayList<LinkedHashMap> QueryObj = (ArrayList<LinkedHashMap>) filters.get("queries");
				
				System.out.println(QueryObj.toString());
				Session session = HibernateUtil.getSessionFactory().openSession();
				
				Query query = null;
				LinkedHashMap searchJObj = null;
				query = session.createQuery("from Request");	
				
					// made for the quick fix, before all the requests where getting returned, filtering based on the location was gone because of commenting the FinderAPI
					if(QueryObj.size() > 1){						
						searchJObj = QueryObj.get(1);	
						if(searchJObj.containsKey("queryText")){
							query = session.createQuery("from Request where lower(title) like :searchString");
							query.setParameter("searchString", "%"+searchJObj.get("queryText").toString().toLowerCase()+"%");	
						}else{
							searchJObj = QueryObj.get(0);
							if(searchJObj.containsKey("queryText")){
								query = session.createQuery("from Request where lower(title) like :searchString");
								query.setParameter("searchString", "%"+searchJObj.get("queryText").toString().toLowerCase()+"%");	
							}
							else{
								query = session.createQuery("from Request");	
							}
						}	
					}else if(QueryObj.size() == 1){
						query = session.createQuery("from Request");	
					}
					
					// this bit of code was implemented for elastic search, which was broke before.. have to look into it in the free time
					//TextQuery searchQueryObj = mapper.convertValue(searchJObj, TextQuery.class); 
					// Filtering was done thorugh elastic search prior			
					//List<Request> response = finderAPI.getResult(query.getQuery(),"request", 0, 100, Request.class);
					
					//Now filtering based on search text

					List<Request> response = query.list();
					System.out.println("GET AVAILABLE REQUEST "+response.size());
					GetRequestBidsService bidsService = new GetRequestBidsService();
	
					for (Request req : response) {
						List<UserBid> bids = bidsService.getUserBids(req.getRequestid(), null);
						requests.add(new UserRequestResponse(req.getRequestid(), req, bids));
					}
				
				
			}
		} catch (Exception e) {
			throw new KraftException(e.getMessage());
		}
		return requests;
	}

}