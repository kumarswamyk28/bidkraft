package com.bidkraft.services.userbids;

import java.io.IOException;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Bid;
import com.bidkraft.entities.PriceLine;
import com.bidkraft.enums.BidStatus;
import com.bidkraft.enums.PriceLineTaggedTo;
import com.bidkraft.enums.PriceLineType;
import com.bidkraft.exception.KraftException;
import com.bidkraft.helpermodels.FeeCutsFromConfigFile;
import com.bidkraft.helpermodels.FinanceInDetail;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.ConfigFileReader;
import com.bidkraft.util.KraftUtil;

/***
 * 
 * @author mgunnam
 * 
 * This service is an upgrade for the existing Create Bid service, the change is that here the business logic is 
 * to get the amount cut from the Service provider side but not from the service seeker side. 
 * 
 * It is right now assumed that the priceline is set for each bid that is raised with all the cuts to be applied 
 *
 */

@Component
@SuppressWarnings("unchecked")
public class CreateBidVersion1Service implements KraftService<Bid> {

	@Override
	public Bid service(KraftRequest request) throws KraftException {
		Bid bidobj = new Bid();
		if (request.getEntities().containsKey(ServiceKeys.CREATEBIDVERSION1)) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transcation = session.beginTransaction();
			try {
				Map<String, String> bidmap = (Map<String, String>) request.getEntities().get(ServiceKeys.CREATEBIDVERSION1);
				System.out.println("This is a create bid version 1 service:");
				bidobj.setRequestId(Long.parseLong(bidmap.get("requestid")));
				bidobj.setDescription(bidmap.get("description"));
				bidobj.setAmount(Double.parseDouble(bidmap.get("amount")));
				bidobj.setUserId(Long.parseLong(bidmap.get("userid")));
				bidobj.setBidDate(KraftUtil.getSQLDate());
				bidobj.setStatus(BidStatus.ACTIVE_BID.getId());

				session.save(bidobj);
			
				// to get the finance object in detail, so as to set the price line object vendor side		
				FinanceInDetail financeInDetObj = getFinanceDetailObject(bidobj);
				//********************* Update Priceline for the Vendor ***********
				
				//Here is the item cost object
				PriceLine itemCostObj = new PriceLine();
				itemCostObj.setAmount(bidobj.getAmount());
				itemCostObj.setRequestid(bidobj.getRequestId());
				itemCostObj.setPriceLineType(PriceLineType.ITEM_COST.getId());
				itemCostObj.setbidid(bidobj.getBidid());
				itemCostObj.setTaggedTo(PriceLineTaggedTo.VENDOR.getId());
				session.save(itemCostObj);

				//Here is fee incurring object
				PriceLine feesObj = new PriceLine();
				feesObj.setAmount(financeInDetObj.getFeeAmount());
				feesObj.setRequestid(bidobj.getRequestId());
				feesObj.setPriceLineType(PriceLineType.FEES.getId());
				feesObj.setbidid(bidobj.getBidid());
				feesObj.setTaggedTo(PriceLineTaggedTo.VENDOR.getId());
				session.save(feesObj);
				
				//Here is total to be charged from ServiceProvider object
				PriceLine totalTobeChargedObj = new PriceLine();
				totalTobeChargedObj.setAmount(financeInDetObj.getTotalAmountToGetFromServiceSeeker());
				totalTobeChargedObj.setRequestid(bidobj.getRequestId());
				totalTobeChargedObj.setPriceLineType(PriceLineType.TOTAL_AMOUNT.getId());
				totalTobeChargedObj.setbidid(bidobj.getBidid());
				totalTobeChargedObj.setTaggedTo(PriceLineTaggedTo.VENDOR.getId());
				session.save(totalTobeChargedObj);
				
				System.out.println("Generated bid id:" + bidobj.getBidid());
				transcation.commit();						
				session.flush();
			} catch (IOException e) {
				
				transcation.rollback();
				throw new KraftException("Exception in GetRequestInfoService " + e.getMessage());
			}
		}
		return bidobj;
	}
	
	
	/**
	 * @param bidObj
	 * @return FinanceInDetail
	 * 
	 * this is the method that does all the calculations like adding the Bidkraft's Cut, Paypal's Cut, Luis's cut over the 
	 * @throws IOException 
	 * 
	 */
	public FinanceInDetail getFinanceDetailObject(Bid bidObj) throws IOException{
		
		FinanceInDetail financeDetailedObj = new FinanceInDetail();
		financeDetailedObj.setBidAmountQuoted(bidObj.getAmount());
		
		// the following segment is to read the 
		
		ConfigFileReader cfReader = new ConfigFileReader();		
		
		FeeCutsFromConfigFile feesObj = cfReader.getFeeCutRealtedObjFromConfig();
		double fees = (bidObj.getAmount() * feesObj.getBidkraftsPerCut())/100;	
		double totalAmount = bidObj.getAmount() + fees;
		
		financeDetailedObj.setFeeAmount(fees);
		financeDetailedObj.setTotalAmountToGetFromServiceSeeker(totalAmount);
		// the following section is for calculating fees amount
		return financeDetailedObj;
	}
	
	
}


































