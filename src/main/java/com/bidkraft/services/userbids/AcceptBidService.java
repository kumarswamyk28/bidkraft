package com.bidkraft.services.userbids;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Bid;
import com.bidkraft.entities.Job;
import com.bidkraft.entities.PriceLine;
import com.bidkraft.enums.BidStatus;
import com.bidkraft.enums.JobStatus;
import com.bidkraft.enums.PriceLineTaggedTo;
import com.bidkraft.enums.PriceLineType;
import com.bidkraft.enums.RequestStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.helpermodels.FeeCutsFromConfigFile;
import com.bidkraft.helpermodels.FinanceInDetail;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.sync.SyncElasticsearch;
import com.bidkraft.util.ConfigFileReader;

@Component
@SuppressWarnings("unchecked")
public class AcceptBidService implements KraftService<String> {
	
	/*@Autowired
	SyncElasticsearch syncElasticsearch;*/

	@Override
	public String service(KraftRequest request) throws KraftException {
		
		if (request.getEntities().containsKey(ServiceKeys.ABID)) {
			Map<String, String> usermap = (Map<String, String>) request.getEntities().get(ServiceKeys.ABID);

			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transcation = session.beginTransaction();
			
			long bidId = Long.parseLong(usermap.get("bidid"));
			long reqId = Long.parseLong(usermap.get("reqid"));
			
			
			//**** First select all the attributes of the Bid, and populate the pricelines for both the vendor and consumer
			Query query = session.createQuery("from Bid where id= :bid");
			query.setParameter("bid", bidId);
			List<Bid> bidEntities = query.list();			
			if(bidEntities.size() > 0) {
				
				Bid bidToBeAccepted = bidEntities.get(0);
			
				//will be changed according to the business rule, right now wanted the Bidkraft's fees to be 10%
				try {
					// Update bid
					Query updateBidStatusQuery = session.createQuery("update Bid set status = :status" + " where id = :bidid");
					updateBidStatusQuery.setParameter("status", BidStatus.WINING_BID.getId());
					updateBidStatusQuery.setParameter("bidid", bidId);
					updateBidStatusQuery.executeUpdate();
	
					// Update Request
					Query updateWinningBidOfReqQuery = session.createQuery("update Request set status = :status,"+ " winningbidid = :winningbidid"  + " where id = :reqid");
					updateWinningBidOfReqQuery.setParameter("status", RequestStatus.AWAITING_PAYMENT.getId());
					updateWinningBidOfReqQuery.setParameter("winningbidid", bidId);
					updateWinningBidOfReqQuery.setLong("reqid", reqId);
					updateWinningBidOfReqQuery.executeUpdate();

					//----------- this segment creates the priceline object for the vendor -------------------------
					
					FinanceInDetail financeInDetObj = getFinanceDetailObject(bidToBeAccepted);
					
					//Here is the item cost object
					PriceLine itemCostObj = new PriceLine();
					itemCostObj.setAmount(bidToBeAccepted.getAmount());
					itemCostObj.setRequestid(bidToBeAccepted.getRequestId());
					itemCostObj.setPriceLineType(PriceLineType.ITEM_COST.getId());
					itemCostObj.setbidid(bidToBeAccepted.getBidid());
					itemCostObj.setTaggedTo(PriceLineTaggedTo.VENDOR.getId());
					session.save(itemCostObj);

					//Here is fee incurring object
					PriceLine feesObj = new PriceLine();
					feesObj.setAmount(financeInDetObj.getFeeAmount());
					feesObj.setRequestid(bidToBeAccepted.getRequestId());
					feesObj.setPriceLineType(PriceLineType.FEES.getId());
					feesObj.setbidid(bidToBeAccepted.getBidid());
					feesObj.setTaggedTo(PriceLineTaggedTo.VENDOR.getId());
					session.save(feesObj);
					
					//Here is total to be charged from ServiceProvider object
					PriceLine totalTobeChargedObj = new PriceLine();
					totalTobeChargedObj.setAmount(financeInDetObj.getTotalAmountToGetFromServiceSeeker());
					totalTobeChargedObj.setRequestid(bidToBeAccepted.getRequestId());
					totalTobeChargedObj.setPriceLineType(PriceLineType.TOTAL_AMOUNT.getId());
					totalTobeChargedObj.setbidid(bidToBeAccepted.getBidid());
					totalTobeChargedObj.setTaggedTo(PriceLineTaggedTo.VENDOR.getId());
					session.save(totalTobeChargedObj);
					
					
					// ----------- this segment creates the Priceline object for the Customer ----------------------
					PriceLine totalAmountObj = new PriceLine();
					// this is the variable that will be set to the amount that customer is responsible for paying to bidkraft
					totalAmountObj.setAmount(financeInDetObj.getBidAmountQuoted());
					totalAmountObj.setRequestid(reqId);
					totalAmountObj.setPriceLineType(PriceLineType.TOTAL_AMOUNT.getId());
					totalAmountObj.setbidid(bidId);
					totalAmountObj.setTaggedTo(PriceLineTaggedTo.CUSTOMER.getId());
					session.save(totalAmountObj);

					//Write to Elastic search, have to make this Elastic search functioning, i guess here the pricelines are been dumped into database here
					//syncElasticsearch.writeToES(reqId);
					transcation.commit();
				} catch (Exception e) {
					transcation.rollback();
					throw new KraftException("Exception in GetRequestInfoService " + e.getMessage());
					//System.out.println(e.getMessage());
				} finally {
					// Commit
					session.flush();
				}
			} else {
				return "{\"error\": \"Please check bidId\"}";
			}
		}
		//return "Success-BidAccepted- PriceLineCreated";
		return "success";

	}
	
	
	
	
	/**
	 * @param bidObj
	 * @return FinanceInDetail
	 * 
	 * this is the method that does all the calculations like adding the Bidkraft's Cut, Paypal's Cut, Luis's cut over the 
	 * @throws IOException 
	 * 
	 */
	public FinanceInDetail getFinanceDetailObject(Bid bidObj) throws IOException{
		
		FinanceInDetail financeDetailedObj = new FinanceInDetail();
		financeDetailedObj.setBidAmountQuoted(bidObj.getAmount());
		
		// the following segment is to read the 
		ConfigFileReader cfReader = new ConfigFileReader();		
		
		FeeCutsFromConfigFile feesObj = cfReader.getFeeCutRealtedObjFromConfig();
		double fees = (bidObj.getAmount() * feesObj.getBidkraftsPerCut())/100;	
		
		// this is the going to be the business rule which sets the total amount that service provider is going to earn
		double totalAmount = bidObj.getAmount() - fees;
		
		financeDetailedObj.setFeeAmount(fees);
		financeDetailedObj.setTotalAmountToGetFromServiceSeeker(totalAmount);
		// the following section is for calculating fees amount
		return financeDetailedObj;
	}
	
}
