package com.bidkraft.services.userbids;

import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Bid;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class UpdateBidService implements KraftService<Bid> {

	@Override
	public Bid service(KraftRequest request) {
		Bid bidobj = new Bid();

		if (request.getEntities().containsKey(ServiceKeys.UBID)) {
			@SuppressWarnings("unchecked")
			Map<String, String> bidmap = (Map<String, String>) request.getEntities().get(ServiceKeys.UBID);

			long bidid = Long.parseLong(bidmap.get("bidid"));
			double amount = Double.parseDouble(bidmap.get("amount"));
			String description = bidmap.get("description");
			String status = bidmap.get("status");
			if (status.equals("INTIALIZED")) {
				Session session = HibernateUtil.getSessionFactory().openSession();

				Query query = session
						.createQuery("update Bid set amount = :amount, description= :description where bidid = :bidid");
				query.setParameter("amount", amount);
				query.setParameter("description", description);
				query.setParameter("bidid", bidid);
				int result = query.executeUpdate();
				System.out.println(result);
				session.flush();
			}
		}

		return bidobj;
	}

}