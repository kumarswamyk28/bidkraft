package com.bidkraft.services.userreviews;

import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Reviews;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class CreateReviewService implements KraftService<Reviews> {

	@Override
	public Reviews service(KraftRequest request) throws KraftException {

		Reviews revobj = new Reviews();

		if (request.getEntities().containsKey(ServiceKeys.CREV)) {
			@SuppressWarnings("unchecked")
			Map<String, String> revmap = (Map<String, String>) request.getEntities().get(ServiceKeys.CREV);

			revobj.setReview_userid(Long.parseLong(revmap.get("review_userid")));
			revobj.setUser_id(Long.parseLong(revmap.get("userid")));

			revobj.setDescription(revmap.get("description"));
			revobj.setRating(Long.parseLong(revmap.get("rating")));

			Long user_id = Long.parseLong(revmap.get("userid"));
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(revobj);

			System.out.println("Generated request id:" + revobj.getReview_id());

			session.getTransaction().commit();
			AverageRating avgrtobj = new AverageRating(user_id);

		}
		return revobj;

	}

}
