package com.bidkraft.services.userreviews;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Request;
import com.bidkraft.entities.Reviews;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class GetUserReviewsService implements KraftService<List<Reviews>> {

	@Override
	public List<Reviews> service(KraftRequest request) throws KraftException {

		List<Reviews> revobj = new ArrayList<Reviews>();

		if (request.getEntities().containsKey(ServiceKeys.GETUSERREVIEWS)) {

			@SuppressWarnings("unchecked")
			Map<String, String> revmap = (Map<String, String>) request.getEntities().get(ServiceKeys.GETUSERREVIEWS);

			Long user_id = Long.parseLong(revmap.get("userid"));

			Session session = HibernateUtil.getSessionFactory().openSession();
			Query query = session.createQuery("from Reviews where user_id= :user_id");

			query.setParameter("user_id", user_id);
			revobj = query.list();

		}
		return revobj;
	}

}
