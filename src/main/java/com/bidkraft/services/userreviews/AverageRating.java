package com.bidkraft.services.userreviews;

import org.hibernate.Transaction;

import org.hibernate.Query;
import org.hibernate.Session;

import com.bidkraft.entities.User;
import com.bidkraft.persistence.HibernateUtil;

public class AverageRating {

	public AverageRating(long userid) {

		try {
			User userobj = new User();
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Query query = session.createQuery("select avg(rating) from Reviews where user_id= :user_id");
			query.setParameter("user_id", userid);

			Double value = (Double) query.uniqueResult();
			System.out.println(value);
		
			Query query1 = session.createQuery("update User set rating = :value where userid= :user_id");
			query1.setParameter("value", 4.00);
			query1.setParameter("user_id", userid);
			
			int rowCount = query1.executeUpdate();
			System.out.println("Rows affected: " + rowCount);
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
