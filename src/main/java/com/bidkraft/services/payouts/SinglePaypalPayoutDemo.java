package com.bidkraft.services.payouts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.services.KraftService;
import com.paypal.api.payments.Currency;
import com.paypal.api.payments.Payout;
import com.paypal.api.payments.PayoutBatch;
import com.paypal.api.payments.PayoutItem;
import com.paypal.api.payments.PayoutSenderBatchHeader;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Component
@SuppressWarnings("unchecked")
public class SinglePaypalPayoutDemo implements KraftService<PayoutBatch>{

	@Override
	public PayoutBatch service(KraftRequest request) throws KraftException {
		// ###Payout
		// A resource representing a payout
		Payout payout = new Payout();
		PayoutBatch payoutBatch = null;
		PayoutSenderBatchHeader senderBatchHeader = new PayoutSenderBatchHeader();

		// ### NOTE:
		// You can prevent duplicate batches from being processed. If you
		// specify a `sender_batch_id` that was used in the last 30 days, the
		// batch will not be processed. For items, you can specify a
		// `sender_item_id`. If the value for the `sender_item_id` is a
		// duplicate of a payout item that was processed in the last 30 days,
		// the item will not be processed.
		// #### Batch Header Instance
		Random random = new Random();
		senderBatchHeader.setSenderBatchId(
				new Double(random.nextDouble()).toString()).setEmailSubject(
				"You have a Payout!");

		// ### Currency
		Currency amount = new Currency();
		amount.setValue("10.00").setCurrency("USD");

		// #### Sender Item
		// Please note that if you are using single payout with sync mode, you
		// can only pass one Item in the request
		PayoutItem senderItem = new PayoutItem();
		senderItem.setRecipientType("Email")
				.setNote("Thanks for your patronage")	// this attributes have to be set dynamically
				.setReceiver("mgunn001-buyer@odu.edu")
				.setSenderItemId("201404324234").setAmount(amount);

		List<PayoutItem> items = new ArrayList<PayoutItem>();
		items.add(senderItem);

		payout.setSenderBatchHeader(senderBatchHeader).setItems(items);
		if (request.getEntities().containsKey(ServiceKeys.SINGLEPAYPALPAYOUT)) {
			try {
	
				// ### Api Context			
				APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");
			
				// ###Create Payout Synchronous
				payoutBatch = payout.createSynchronous(apiContext);
				System.out.println("lastRequest:" + Payout.getLastRequest());
				System.out.println("lastRequest:" + Payout.getLastResponse());
			} catch (PayPalRESTException ppre) {
				KraftException ke = new KraftException(ppre.getMessage());
				throw ke;
			}
		}
		System.out.println("lastRequest:" + payoutBatch);

		return payoutBatch;
	}

}
