/*package com.bidkraft.services.payouts;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Distribution;
import com.bidkraft.entities.DistributionPayoutResponse;
import com.bidkraft.entities.Job;
import com.bidkraft.entities.PriceLine;
import com.bidkraft.entities.User;
import com.bidkraft.enums.DistributionStatus;
import com.bidkraft.enums.PriceLineTaggedTo;
import com.bidkraft.enums.PriceLineType;
import com.bidkraft.exception.KraftException;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.responses.FPInitiationResponse;
import com.paypal.api.payments.Currency;
import com.paypal.api.payments.Payout;
import com.paypal.api.payments.PayoutBatch;
import com.paypal.api.payments.PayoutItem;
import com.paypal.api.payments.PayoutSenderBatchHeader;
import com.paypal.base.rest.APIContext;

public class SinglePaypalPayoutWithScheduling implements org.quartz.Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transcation = session.beginTransaction();
		FPInitiationResponse fpInitiationRespObj = new FPInitiationResponse();		
		String clientMetaDataId = "";
		Long curJobId,curReqid;

			Map<String, String> singlePayoutIPMap = (Map<String, String>) request.getEntities().get(ServiceKeys.SINGLEPAYPALPAYOUT);
			System.out.println("This is the service to process the Future Payment Paypal :PayByFuturePayment");
			Long providerId = Long.parseLong(singlePayoutIPMap.get("userid"));
			curJobId = Long.parseLong(singlePayoutIPMap.get("jobid"));
			
			//** this one is to get the cur job details 
			Query getJobDetailsByIDQuery = session.createQuery("from Job where job_id = :job_id");
			getJobDetailsByIDQuery.setParameter("job_id", curJobId);					
			Job curJobObj = (Job)getJobDetailsByIDQuery.list().get(0);
			
			//** this one is to get the Amount details for the payout to be initiated
			Query getPriceLineAmtQuery = session.createQuery("from PriceLine where bidid = :bid and requestid = :reqid and priceLineType = :plType and taggedTo = :taggedTo");
			getPriceLineAmtQuery.setParameter("bid", curJobObj.getBid_id());
			getPriceLineAmtQuery.setParameter("reqid", curJobObj.getRequest_id());
			getPriceLineAmtQuery.setParameter("plType", PriceLineType.TOTAL_AMOUNT.getId());
			getPriceLineAmtQuery.setParameter("taggedTo", PriceLineTaggedTo.VENDOR.getId());			
			PriceLine priceLineObj = (PriceLine)getPriceLineAmtQuery.list().get(0);

			
			//** this one is to get the user details 
			Query getUserDetailsByIDQuery = session.createQuery("from User where userid = :userId");
			getUserDetailsByIDQuery.setParameter("userId", providerId);					
			User curUserObj = (User)getJobDetailsByIDQuery.list().get(0);
			

			// ###Payout
			Payout payout = new Payout();
			PayoutBatch payoutBatch = null;
			PayoutSenderBatchHeader senderBatchHeader = new PayoutSenderBatchHeader();

			// #### Batch Header Instance
			Random random = new Random();
			senderBatchHeader.setSenderBatchId(
					new Double(random.nextDouble()).toString()).setEmailSubject("You have a Payout!");

			// ### Currency
			Currency amount = new Currency();
			amount.setValue(Double.toString(Math.round(priceLineObj.getAmount()))).setCurrency("USD");

			// #### Sender Item
			// Please note that if you are using single payout with sync mode, you
			// can only pass one Item in the request
			PayoutItem senderItem = new PayoutItem();
			senderItem.setRecipientType("Email")
					.setNote("Thanks for your patronage")	// this attributes have to be set dynamically
					.setReceiver(curUserObj.getPaypalRegisteredEmail())
					.setSenderItemId("201404324234").setAmount(amount); // here sender item id has to be randomly generated and must be unique as well

			List<PayoutItem> items = new ArrayList<PayoutItem>();
			items.add(senderItem);
			payout.setSenderBatchHeader(senderBatchHeader).setItems(items);
			
			try {
					// ### Api Context			
					APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");
				
					// ###Create Payout Synchronous
					payoutBatch = payout.createSynchronous(apiContext);
					System.out.println("lastRequest:" + Payout.getLastRequest());
					System.out.println("lastRequest:" + Payout.getLastResponse()); // this is the response that we get out of an payout 
					
					//once the payout response is got, now an entry has to be made in both the Distribution and Distribution_PayoutResponse
					JSONObject paypalPayoutRespJObj = new JSONObject(Payout.getLastResponse().toString());
					JSONObject paypalBatchHeaderJsonObj= paypalPayoutRespJObj.getJSONObject("batch_header");
					JSONObject paypalSenderBatchHeaderJsonObj= paypalPayoutRespJObj.getJSONObject("sender_batch_header");						
					
					// this one is to set the Distribution object
					Distribution distObj = new Distribution();
					distObj.setJobid(curJobObj.getJob_id());
					distObj.setSellerid(curUserObj.getUserid());
					
					if(paypalBatchHeaderJsonObj.getString("batch_status") == "SUCCESS"){
						distObj.setDistributionstatus(DistributionStatus.SUCCESS.getId());
					}else{
						distObj.setDistributionstatus(DistributionStatus.NOTSUCCESS.getId()); // this has to be changed knowing what more status can be possible
					}
					distObj.setAmount(priceLineObj.getAmount());
					session.save(distObj);
						
						//this one is to set the Distribution Payout Response
					DistributionPayoutResponse distPayoutRes = new DistributionPayoutResponse();
					distPayoutRes.setBatchStatus(DistributionStatus.PENDING.getName());
					distPayoutRes.setDistributionId(distObj.getDistributionid());
					distPayoutRes.setCreatedDateTime(new Timestamp(System.currentTimeMillis()));
					distPayoutRes.setWholeResponse(Hibernate.createBlob(Payout.getLastResponse().toString().getBytes()));
					distPayoutRes.setSenderBatchId(paypalSenderBatchHeaderJsonObj.getLong("sender_batch_id") );
					distPayoutRes.setPayoutResponseBatchId(paypalBatchHeaderJsonObj.getString("payout_batch_id"));
					session.save(distPayoutRes);
					
				} catch (Exception e) {
					KraftException ke = new KraftException(e.getMessage());
					throw ke;
				}
			System.out.println("lastRequest:" + payoutBatch);
			return payoutBatch;

	}
	

}
*/