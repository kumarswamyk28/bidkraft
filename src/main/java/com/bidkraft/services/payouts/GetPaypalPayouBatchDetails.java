package com.bidkraft.services.payouts;



import org.springframework.stereotype.Component;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.services.KraftService;
import com.paypal.api.payments.Payout;
import com.paypal.api.payments.PayoutBatch;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Component
@SuppressWarnings("unchecked")
public class GetPaypalPayouBatchDetails implements KraftService<PayoutBatch >{

	@Override
	public PayoutBatch service(KraftRequest request) throws KraftException { // this is nothing but all the distribution history
		PayoutBatch payoutBatchDet = new PayoutBatch();
		
		if (request.getEntities().containsKey(ServiceKeys.GETPAYPALPAYOUTBATCHDET)) {
			// to convert the request json string to map object			
			try {
	
				// ### Api Context
				APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");
	
				// ###Get Payout Batch Status
				String payoutBatchId="H2GLC5DZX8882"; // this has to be ideally from some other service
				payoutBatchDet = Payout.get(apiContext, payoutBatchId);
				System.out.println(Payout.getLastResponse().toString());		
				
			} catch (PayPalRESTException ppre) {
				KraftException ke = new KraftException(ppre.getMessage());
				throw ke;	
			}
		
		}
		return payoutBatchDet;
	} 

}
