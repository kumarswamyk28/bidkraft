package com.bidkraft.services.payments;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;



import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.enums.CollectionStatus;
import com.bidkraft.enums.JobStatus;
import com.bidkraft.enums.PriceLineTaggedTo;
import com.bidkraft.enums.PriceLineType;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.paypalutil.PaypalPaymentUtil;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.responses.FPInitiationResponse;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.HttpResponseReader;
import com.mysql.jdbc.Blob;
import com.bidkraft.entities.*;


/**
 * @author mgunnam
 * 
 * this method receives the amount, clientmetadataid and userid to process the payment from client to bidkraft
 *  Technically in the paypal terms what takes place here is the Authorization for the payment (Initiation of the payment - keeping hold on the particular amount)
 *  
 *  In this service the tables which are updated are futuretranxauthorixation,collection and collection_paymentresponse
 */
@Component
@SuppressWarnings("unchecked")
public class InitiatePaypalFuturePayment implements KraftService<FPInitiationResponse>{
		
	
	@SuppressWarnings("deprecation")
	@Override
	public FPInitiationResponse service(KraftRequest request) throws KraftException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transcation = session.beginTransaction();
		FPInitiationResponse fpInitiationRespObj = new FPInitiationResponse();		
		String clientMetaDataId = "";
		Long curBidId,curReqid;
		if (request.getEntities().containsKey(ServiceKeys.INITIATEPAYPALFUTUREPAYMENT)) {
		

			Map<String, String> futurePaymentIPMap = (Map<String, String>) request.getEntities().get(ServiceKeys.INITIATEPAYPALFUTUREPAYMENT);
			System.out.println("This is the service to process the Future Payment Paypal :PayByFuturePayment");
			Long userId = Long.parseLong(futurePaymentIPMap.get("userid"));
			fpInitiationRespObj.setUserId(userId);			
			clientMetaDataId = futurePaymentIPMap.get("clientmetadataid");	
			curBidId = Long.parseLong(futurePaymentIPMap.get("bidid"));
			curReqid = Long.parseLong(futurePaymentIPMap.get("requestid"));
			
			System.out.println("\n**********\nClientMetaDataId: "+ clientMetaDataId);	
			
			//** this one is to get the Amount details for the future payment to be initiated
			Query getPriceLineAmtQuery = session.createQuery("from PriceLine where bidid = :bid and requestid = :reqid and priceLineType = :plType and taggedTo = :taggedTo");
			getPriceLineAmtQuery.setParameter("bid", curBidId);
			getPriceLineAmtQuery.setParameter("reqid", curReqid);
			getPriceLineAmtQuery.setParameter("plType", PriceLineType.TOTAL_AMOUNT.getId());
			getPriceLineAmtQuery.setParameter("taggedTo", PriceLineTaggedTo.CUSTOMER.getId());

			List<PriceLine> priceLineEntities = getPriceLineAmtQuery.list();
			
			if(priceLineEntities.size() > 0) {
				fpInitiationRespObj.setAmount(priceLineEntities.get(0).getAmount());
			}
			
			
			// this util object has some usefull methods like RefreshedAccessToken and all 
			PaypalPaymentUtil paypalPaymentUtil = new PaypalPaymentUtil();		
			FutureTranxAuthorization futureTranxAutObj = paypalPaymentUtil.getFutureTranxAutObjByUserId(userId);
			
			// here the refreshed access token are stored back in DB						
			
			try{

				JSONObject acessRefreshTokenJObj = new JSONObject();
				//refreshed access tokens are get here, using which the payment
				acessRefreshTokenJObj = paypalPaymentUtil.getRefreshedAccessTokenObj(futureTranxAutObj.getRefreshToken());
				Timestamp lastRefreshedDateTime = new Timestamp(System.currentTimeMillis());
				
				// Store the ClientMetadaid, refreshed access_token details in the FutureTranxAuthorization	(Correct this, update 
				Query updateRefreshedAccessAndCLientMetaQuery = session.createQuery("update FutureTranxAuthorization set accessToken = :accessToken, expiresIn = :expiresIn, clientMetaData_id = :clientMetaDataId, lastRefreshedDateTime = :lastRefreshedDateTime where user_id = :userId");
				updateRefreshedAccessAndCLientMetaQuery.setParameter("userId", userId);
				updateRefreshedAccessAndCLientMetaQuery.setParameter("accessToken", acessRefreshTokenJObj.getString("access_token"));
				updateRefreshedAccessAndCLientMetaQuery.setParameter("expiresIn", Long.parseLong(acessRefreshTokenJObj.getString("expires_in")));
				updateRefreshedAccessAndCLientMetaQuery.setParameter("clientMetaDataId", clientMetaDataId);
				updateRefreshedAccessAndCLientMetaQuery.setParameter("lastRefreshedDateTime", lastRefreshedDateTime);
				updateRefreshedAccessAndCLientMetaQuery.executeUpdate();
			

				// call this method to initiate the payment
				HttpResponse response =  createFuturePaymentThroughHTTP(acessRefreshTokenJObj.getString("access_token"),userId,clientMetaDataId,fpInitiationRespObj.getAmount());
				HttpResponseReader httpResponseReader = new HttpResponseReader();
				String httpRespStr =  httpResponseReader.readHttpResponseContent(response);
				JSONObject fpPaypalResponseJObj = new JSONObject(httpRespStr);
				String initialPaymentStatus = fpPaypalResponseJObj.getString("state");
				// read the response got to store the attributes 
				//if(initialPaymentStatus.equals("approved")){
					// call the completeFuturePaymentThroughHTTP here to capture the Amount 				
					//this captureLink has to be set by iterating through the Json returned from create Payment method
					JSONArray responseLinksObjList =  fpPaypalResponseJObj.getJSONArray("transactions");	
					
					JSONArray relatedResourcesList = ((JSONObject)responseLinksObjList.get(0)).getJSONArray("related_resources");			

					JSONObject authorizationObj= ((JSONObject) relatedResourcesList.get(0)).getJSONObject("authorization");	
					
					JSONArray LinksList = authorizationObj.getJSONArray("links");
					
					/*String captureLink ="";
					for (int i = 0; i < LinksList.length(); i++) {					
						JSONObject jsonObj = LinksList.getJSONObject(i);
						  if(jsonObj.getString("rel").equals("capture") ){
							  captureLink= jsonObj.getString("href");
							  break;
						  }					  
					}			
					System.out.println("\n**************\nCaptureLink for capturing the payment  : " +captureLink);
					// store the response attributes in to Capture Tables */					

				
					/**
					 * Once the response is get now the job object has to be created followed by both the Collection and Collection_Response Objects are to be set, that code is as follows:
					 */
					Job jobObj = new Job();
					jobObj.setBid_id(curBidId);
					jobObj.setRequest_id(curReqid);
					jobObj.setStatus(JobStatus.INITIATED.getId());
					session.save(jobObj);
									
					Collection collectionObj = new Collection();
					collectionObj.setAmount(fpInitiationRespObj.getAmount());
					collectionObj.setBuyerid(userId);
					if(initialPaymentStatus.equals("approved")){
						collectionObj.setCollectionstatus(CollectionStatus.APPROVED.getId());
						fpInitiationRespObj.setPaymentStatus("approved");
					}else{
						fpInitiationRespObj.setPaymentStatus("initated");
						collectionObj.setCollectionstatus(CollectionStatus.INITIATED.getId());
					}
					collectionObj.setJobid(jobObj.getJob_id());
					collectionObj.setClientMetaDataId(clientMetaDataId);
					session.save(collectionObj);
					
					CollectionPaymentResponse collPaymentRes = new CollectionPaymentResponse();
					collPaymentRes.setResponseId(fpPaypalResponseJObj.getString("id"));
					collPaymentRes.setCreatedDateTime(new Timestamp(System.currentTimeMillis()));
					collPaymentRes.setCollectionId(collectionObj.getCollectionid());
					collPaymentRes.setIntent(fpPaypalResponseJObj.getString("intent"));
					collPaymentRes.setParentPaymentId(authorizationObj.getString("parent_payment"));
					collPaymentRes.setAuthorizationId(authorizationObj.getString("id"));
					collPaymentRes.setState(initialPaymentStatus);
					
					collPaymentRes.setWholeResponse(Hibernate.createBlob(httpRespStr.getBytes()));
					session.save(collPaymentRes);
					transcation.commit();
					session.flush();
					
				String captureResState =  captureFuturePaymentThroughHTTP(collectionObj.getJobid(),collectionObj.getBuyerid(),collectionObj.getCollectionid());				
				fpInitiationRespObj.setPaymentStatus(captureResState);
			} catch (Exception e) {
				transcation.rollback();
				// TODO: handle exception
				KraftException ke = new KraftException(e.getMessage());
				throw ke;
			}

		}
		return fpInitiationRespObj;
	}
	
	
	
	
	// this method is for the Creating FuturePayments through the Http calls, here the authorization for certain amount is done and this amount is put on hold
	@SuppressWarnings("deprecation")
	public HttpResponse createFuturePaymentThroughHTTP(String accessToken,long userId,String clientMetaDataId,Double amount) throws Exception {	
			
			String url = "https://api.sandbox.paypal.com/v1/payments/payment";
			@SuppressWarnings("resource")
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			
			// add header	
			post.setHeader("Content-Type", "application/json");
			post.setHeader("PayPal-Client-Metadata-Id", clientMetaDataId);
			post.setHeader("Authorization", "Bearer "+accessToken);
			
			
			//create the json object to set to the request
			String inputReqObjStr = "{\"intent\":\"authorize\",\"payer\":{\"payment_method\":\"paypal\"},\"transactions\":[{\"amount\":{\"currency\":\"USD\",\"total\":\""+Math.round(amount)+"\"},\"description\":\"future of sauces\"}]}";
			
			StringEntity inputReqObjEntity = new StringEntity(inputReqObjStr);			
			post.setEntity(inputReqObjEntity);
			HttpResponse response = client.execute(post);

			System.out.println("\n*** Creating the Payment**** Sending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + post.getEntity());
			System.out.println("Response Code : " +
	                                    response.getStatusLine().getStatusCode());

			return response;
	}
	
	
	
	// this method is for caputing the payments as the final capture
		@SuppressWarnings("deprecation")
		public String captureFuturePaymentThroughHTTP(long jobId,long buyerId,long collectionId) throws Exception {	
			
			PaypalPaymentUtil paypalPaymentUtil = new PaypalPaymentUtil();		
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transcation = session.beginTransaction();
			
			
			// to get the Collection Object, after which will have to work on getting the Collection response object (usually this has to be a join query)
			Query getCollectionObjQuery = session.createQuery("from Collection where collectionid = :collectionId and jobid = :jobId and buyerid = :buyerId");
			getCollectionObjQuery.setParameter("collectionId", collectionId);
			getCollectionObjQuery.setParameter("jobId", jobId);
			getCollectionObjQuery.setParameter("buyerId",buyerId);
			Collection collectionObj = (Collection) getCollectionObjQuery.list().get(0);
			
			
			// this query is basically to get the CollectionPaymentResponseObject related to Authorize, to get the link to finalize the data
			Query getCollPayResObjQuery = session.createQuery("from CollectionPaymentResponse where collectionId = :collectionId and state =:state and intent =:intent");
			getCollPayResObjQuery.setParameter("collectionId", collectionId);
			getCollPayResObjQuery.setParameter("state", CollectionStatus.APPROVED.getName());
			getCollPayResObjQuery.setParameter("intent","authorize");
			
			CollectionPaymentResponse collPaymentRespObj = (CollectionPaymentResponse) getCollPayResObjQuery.list().get(0);

			FutureTranxAuthorization futureTranxAutObj = paypalPaymentUtil.getFutureTranxAutObjByUserId(buyerId);			
			// here the refreshed access token are stored back in DB						
			JSONObject acessRefreshTokenJObj = new JSONObject();
			//refreshed access tokens are get here, using which the payment
			acessRefreshTokenJObj = paypalPaymentUtil.getRefreshedAccessTokenObj(futureTranxAutObj.getRefreshToken());
			

			String url = "https://api.sandbox.paypal.com/v1/payments/authorization/"+collPaymentRespObj.getAuthorizationId()+"/capture";
			
			@SuppressWarnings("resource")
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);		
			// add header	
			post.setHeader("Content-Type", "application/json");
			post.setHeader("PayPal-Client-Metadata-Id", collectionObj.getClientMetaDataId());
			post.setHeader("Authorization", "Bearer "+ acessRefreshTokenJObj.getString("access_token"));
			
			
			//create the json object to set to the request
			String inputReqObjStr = "{\"amount\":{\"currency\":\"USD\",\"total\":\""+ Math.round(collectionObj.getAmount())+"\"},\"is_final_capture\":true}";
			
			StringEntity inputReqObjStrEntity = new StringEntity(inputReqObjStr);			
			post.setEntity(inputReqObjStrEntity);
			HttpResponse response = client.execute(post);

			System.out.println("\n*** Capturing the Payment **** Sending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + post.getEntity());
			System.out.println("Response Code : " +response.getStatusLine().getStatusCode());
			
			// this following code reads the capture payment response and stores it back in DB in CollectionResponsePayment
			HttpResponseReader httpResponseReader = new HttpResponseReader();
			String json_string =  httpResponseReader.readHttpResponseContent(response);
			JSONObject capturePaymentPaypalRespJObj = new JSONObject(json_string);

			CollectionPaymentResponse collCapturePaymentRes = new CollectionPaymentResponse();
			collCapturePaymentRes.setResponseId(capturePaymentPaypalRespJObj.getString("id"));
			collCapturePaymentRes.setCreatedDateTime(new Timestamp(System.currentTimeMillis()));
			collCapturePaymentRes.setCollectionId(collectionObj.getCollectionid());
			//as this row is for final capture
			collCapturePaymentRes.setIntent("finalcapture");
			collCapturePaymentRes.setParentPaymentId(capturePaymentPaypalRespJObj.getString("parent_payment"));
			collCapturePaymentRes.setState(capturePaymentPaypalRespJObj.getString("state"));			
			collCapturePaymentRes.setWholeResponse(Hibernate.createBlob(json_string.getBytes()));
			session.save(collCapturePaymentRes);

			
			if(capturePaymentPaypalRespJObj.getString("state").equals("completed")){
				//update collection object with the status as completed
				Query updateCollOnCaptureResQuery = session.createQuery("update Collection set collectionstatus = :collectionstatus where collectionid = :collectionid");
				updateCollOnCaptureResQuery.setParameter("collectionstatus", CollectionStatus.COMPLETED.getId());
				updateCollOnCaptureResQuery.setParameter("collectionid", collectionObj.getCollectionid());
				updateCollOnCaptureResQuery.executeUpdate();
			}

			transcation.commit();
			session.flush();
			return capturePaymentPaypalRespJObj.getString("state");

		}
	
}
