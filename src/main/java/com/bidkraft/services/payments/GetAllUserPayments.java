package com.bidkraft.services.payments;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.UserPayment;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
@SuppressWarnings("unchecked")
public class GetAllUserPayments implements KraftService<List<UserPayment>>{

	private APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");
	
	@Override
	public List<UserPayment> service(KraftRequest request) throws KraftException {
		List<UserPayment>  userPayments = new ArrayList<UserPayment>();
		
		// to check is this service exists or not
		if (request.getEntities().containsKey(ServiceKeys.GETALLUSERPAYMENTS)) {
			// to convert the request json string to map object
			Map<String, String> requestMap = (Map<String, String>) request.getEntities()
					.get(ServiceKeys.GETALLUSERPAYMENTS);

			Long userid = Long.parseLong(requestMap.get("userid"));
			Session session = HibernateUtil.getSessionFactory().openSession();
			Query query = session.createQuery("from UserPayment where seekerUserId= :jobseekerid");
			query.setParameter("jobseekerid", userid);
			userPayments = query.list();	
		}
		return userPayments;
	}
	
	
	// this method is to return the payment object from the paypal api by paymentID
	public Payment getPaypalPaymentById(String paymentId) throws KraftException {
		Payment payment = null;
		try {	
			payment = Payment.get(apiContext,paymentId);
			//LOGGER.info	
		} catch (PayPalRESTException ppre) {
			KraftException ke = new KraftException(ppre.getMessage());
			throw ke;
		}
		return payment;
	}
	
	
	
	
	
	
	
	

}
