package com.bidkraft.services.payments;

import com.paypal.api.payments.CreditCard;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

import java.util.List;

import org.springframework.stereotype.Component;



import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.services.KraftService;


@Component
@SuppressWarnings("unchecked")
public class StoreCreditCardPaypalVault implements KraftService<CreditCard>{ 
	//** this method is to store the Credit card details at the Paypal End, which then gives us the unique id which can be used for the further payments.
	//* refer the url --- https://github.com/paypal/PayPal-Java-SDK/blob/master/rest-api-sample/src/main/java/com/paypal/api/payments/servlet/CreateCreditCardServlet.java
	
	@SuppressWarnings("deprecation")
	@Override
	public CreditCard service(KraftRequest request) throws KraftException {
		
		// ### Api Context			
		  APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");

		// Create a Credit Card
		CreditCard card = new CreditCard()
		  .setType("visa")
		  .setNumber("4417119669820331")
		  .setExpireMonth(11)
		  .setExpireYear(2019)
		  .setCvv2(012)
		  .setFirstName("Joe")
		  .setLastName("Shopper");
		CreditCard createdCreditCard = null;
		try {
			createdCreditCard = card.create(apiContext);
		  System.out.println("creditcard created with Id: "+ createdCreditCard.getId());
		 
		} catch (PayPalRESTException e) {
		  System.err.println(e.getDetails());
		}
	
		 return createdCreditCard;
	}
}



