package com.bidkraft.services.payments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.services.KraftService;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Component
@SuppressWarnings("unchecked")
public class InitiatePaypalPayment implements KraftService<Payment>{

	@Override
	public Payment service(KraftRequest request) throws KraftException {

			Payer payer = new Payer();	
			payer.setPaymentMethod("paypal");
			
			//Have to properly set these redirection URLs
			RedirectUrls redirectUrls = new RedirectUrls();
			redirectUrls.setCancelUrl("http://localhost:8080/JaxRSDemo/resources/requestcancelled");
			redirectUrls.setReturnUrl("http://localhost:8080/JaxRSDemo/resources/requestunderprocess");
	
			// Set payment details
			Details details = new Details();
			details.setShipping("1");
			details.setSubtotal("5");
			details.setTax("1");
	
			// Payment amount
			Amount amount = new Amount();
			amount.setCurrency("USD");
			// Total must be equal to sum of shipping, tax and subtotal.
			amount.setTotal("7");
			amount.setDetails(details);
	
			// Transaction information
			Transaction transaction = new Transaction();
			transaction.setAmount(amount);
			transaction.setDescription("This is a test transaction through Paypal.");
	
			// Add transaction to a list
			List<Transaction> transactions = new ArrayList<Transaction>();
			transactions.add(transaction);
	
			// Add payment details
			Payment payment = new Payment();
			payment.setIntent("sale");
			payment.setPayer(payer);
			payment.setRedirectUrls(redirectUrls);
			payment.setTransactions(transactions);
			
			Payment createdPayment = null;
			if (request.getEntities().containsKey(ServiceKeys.INITIATEPAYPALPAYMENT)) {
				
				try{			
					 // ### Api Context			
					 APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");
					
					 createdPayment = payment.create(apiContext);
					 Iterator<Links> links = createdPayment.getLinks().iterator();
					 while (links.hasNext()) {
					    Links link = links.next();
					    if (link.getRel().equalsIgnoreCase("approval_url")) {
					    	System.out.println("Inside the approve URL");
					    	// this is the link that gets the credentials giving screen
					      // REDIRECT USER TO link.getHref()
					    }
					  }
				} catch (PayPalRESTException e) {
				    System.err.println(e.getDetails());
				}
			}
			
			return createdPayment;
	}
	
	
	
	
}
