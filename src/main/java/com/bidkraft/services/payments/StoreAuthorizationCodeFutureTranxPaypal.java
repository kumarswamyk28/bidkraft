package com.bidkraft.services.payments;


import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;

import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.FutureTranxAuthorization;
import com.bidkraft.entities.User;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.paypalutil.PaypalPaymentUtil;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.enums.UserPayPalAuthStatus;


@Component
@SuppressWarnings("unchecked")
public class StoreAuthorizationCodeFutureTranxPaypal implements KraftService<FutureTranxAuthorization>{

	@Override
	public FutureTranxAuthorization service(KraftRequest request) throws KraftException {
		/** this service sets the Client Generated AuthCode(will not be updated again), 
		 * Expiresin(will be updated), Refresh Token,AccessToken(this gets updated for every payment initiation),
		 * ClientMetaData_id of this related table will be update when ever a payment happen,
		 * lastrefresheddatetime is updated when ever the access token is refreshed.
		 * For now this method is returning all the properties, a decision has to be made whether to send every thing or just consent success
		*/
				
		FutureTranxAuthorization futureTranxAuthObj = new FutureTranxAuthorization();
		
		if (request.getEntities().containsKey(ServiceKeys.STOREFUTURETRANXAUTHKEY)) {		
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Map<String, String> futureTranxAuthMap = (Map<String, String>) request.getEntities().get(ServiceKeys.STOREFUTURETRANXAUTHKEY);
			System.out.println("This is the service to store the Future Transaction Authentication key:");			
			//now the auth code that is sent from the mobile client has to be exchanged with Refreshed/Access Tokens		
			
			JSONObject jObj = new JSONObject();			
			try{
				PaypalPaymentUtil paypalPaymentUtil = new PaypalPaymentUtil();				
				jObj =paypalPaymentUtil.getAccessAndRefreshToken(futureTranxAuthMap.get("authkey"));	
				Timestamp lastRefreshedDateTime = new Timestamp(System.currentTimeMillis());
				Long userId = Long.parseLong(futureTranxAuthMap.get("userid"));
				User user;
				// to see whether this particular user is being consented or not.
				Query getUserDetailQuery = session.createQuery("from User where userid= :userId ORDER BY id DESC");
				getUserDetailQuery.setParameter("userId", userId);
				user = (User)getUserDetailQuery.list().get(0);

				if(user.getIsPayPalAuth().equals(UserPayPalAuthStatus.NO.getName())){
					// save the FutureTranxAuth object here (Insert New), other wise update.
					
					futureTranxAuthObj.setAuthCode(futureTranxAuthMap.get("authkey"));
					futureTranxAuthObj.setUser_id(userId);		
					futureTranxAuthObj.setCreatedDateTime(new Timestamp(System.currentTimeMillis()));
					futureTranxAuthObj.setRefreshToken(jObj.getString("refresh_token"));
					futureTranxAuthObj.setAccessToken(jObj.getString("access_token"));
					futureTranxAuthObj.setExpiresIn(jObj.getLong("expires_in"));				
					futureTranxAuthObj.setLastRefreshedDateTime(lastRefreshedDateTime);
					session.save(futureTranxAuthObj);
					System.out.println("Generated futureTranxAuth id:" + futureTranxAuthObj.getFutureTranxAuth_id());					
					//Query to update the user that he is consented (ispaypalauth is set to YES)
					Query updateUserPaypalConsentAuthQuery = session.createQuery("update User set isPayPalAuth = :ispaypalauth where userid = :userId");
					updateUserPaypalConsentAuthQuery.setParameter("userId", userId);
					updateUserPaypalConsentAuthQuery.setParameter("ispaypalauth", UserPayPalAuthStatus.YES.getName());
					updateUserPaypalConsentAuthQuery.executeUpdate();				
					
				}else{
					// updating the Future Transaction takes place here.					
					Query updateFutureTransactionAuthQuery = session.createQuery("update FutureTranxAuthorization set accessToken = :accessToken, refreshToken = :refreshToken, expiresIn = :expiresIn, lastRefreshedDateTime = :lastRefreshedDateTime where user_id = :userId");
					updateFutureTransactionAuthQuery.setParameter("userId", userId);
					updateFutureTransactionAuthQuery.setParameter("refreshToken", jObj.getString("refresh_token"));
					updateFutureTransactionAuthQuery.setParameter("accessToken", jObj.getString("access_token"));
					updateFutureTransactionAuthQuery.setParameter("expiresIn", jObj.getLong("expires_in"));					
					updateFutureTransactionAuthQuery.setParameter("lastRefreshedDateTime", lastRefreshedDateTime);
					updateFutureTransactionAuthQuery.executeUpdate();
				}
				session.getTransaction().commit();
				
			}catch (Exception e) {
				session.getTransaction().rollback();
				// TODO: handle exception
				KraftException ke = new KraftException(e.getMessage());
				throw ke;
			}
			session.flush();
		}
		return futureTranxAuthObj;
	} 
	

}
























