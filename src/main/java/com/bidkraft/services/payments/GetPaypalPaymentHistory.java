package com.bidkraft.services.payments;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.services.KraftService;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentHistory;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Component
@SuppressWarnings("unchecked")
public class GetPaypalPaymentHistory implements KraftService<PaymentHistory >{ // this is nothing but all the collection history

	@Override
	public PaymentHistory  service(KraftRequest request) throws KraftException {
		PaymentHistory paymentHistory = new PaymentHistory();
		
		// to check is this service exists or not
		if (request.getEntities().containsKey(ServiceKeys.GETPAYPALPAYMENTHISTORY)) {
			// to convert the request json string to map object
			
			Map<String, String> containerMap = new HashMap<String, String>();
			containerMap.put("count", "10");
	
			try {
				// ### Api Context
				APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");

				// ###Retrieve
				 paymentHistory = Payment.list(apiContext,containerMap);	
				 System.out.println(Payment.getLastResponse().toString());
				 System.out.println("end of coding section of Payment Histroy.");
			}
			catch(PayPalRESTException ppre){				
				KraftException ke = new KraftException(ppre.getMessage());
				throw ke;				
			}

		}
		return paymentHistory;
	}

}