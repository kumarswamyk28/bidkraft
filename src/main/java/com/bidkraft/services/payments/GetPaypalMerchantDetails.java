package com.bidkraft.services.payments;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.UserPayment;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.responses.PaypalMerchantDetResponse;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.ConfigFileReader;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
@SuppressWarnings("unchecked")
public class GetPaypalMerchantDetails implements KraftService<PaypalMerchantDetResponse>{

	@Override
	public PaypalMerchantDetResponse service(KraftRequest request) throws KraftException {
		// TODO Auto-generated method stub
		
		PaypalMerchantDetResponse paypalMerchDet = null;
		
		if (request.getEntities().containsKey(ServiceKeys.GETPAYPALMERCHANTDETAILS)) {
			// to convert the request json string to map object
			Map<String, String> requestMap = (Map<String, String>) request.getEntities()
					.get(ServiceKeys.GETPAYPALMERCHANTDETAILS);
			Long userid = Long.parseLong(requestMap.get("userid"));
			System.out.println("user currently asking for Merchant Details: "+userid);
			 paypalMerchDet= new PaypalMerchantDetResponse(PaymentPaypalKeys.merchantId,PaymentPaypalKeys.merchantSecret);
			
			 
			/* just testing
			 * try{
				 ConfigFileReader cfReader = new ConfigFileReader();
					double bidKraftCutPer = cfReader.getBidKraftCut();
			} catch(Exception e){
				e.printStackTrace();
			}*/
			
				
		}
		
		return paypalMerchDet;	
	}

	
}
