package com.bidkraft.services.payments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.PriceLine;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
@SuppressWarnings("unchecked")
public class GetUserPaymentPriceLines implements KraftService<List<PriceLine>> {

	@Override
	public List<PriceLine> service(KraftRequest request) throws KraftException {
		List<PriceLine> userPaymentPriceLines = new ArrayList<PriceLine>();

		if (request.getEntities().containsKey(ServiceKeys.GETUSERPAYMENTPRICELINES)) {
			// to convert the request json string to map object
			Map<String, String> requestMap = (Map<String, String>) request.getEntities()
					.get(ServiceKeys.GETUSERPAYMENTPRICELINES);

			long jobId = Long.parseLong(requestMap.get("jobid"));
			Session session = HibernateUtil.getSessionFactory().openSession();
			Query getReqIdQuery = session.createQuery("select request_id from Job where job_id= :jobid");		
			getReqIdQuery.setParameter("jobid", jobId);			
			long reqId = (long) getReqIdQuery.uniqueResult();	
			Query getPriceListQuery = session.createQuery("from PriceLine where requestid= :request_id");
			getPriceListQuery.setParameter("request_id", reqId);
			userPaymentPriceLines = (ArrayList<PriceLine>)getPriceListQuery.list();		
		}
		return userPaymentPriceLines;
	}

}
