package com.bidkraft.services.payments;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Iterator;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.FuturePayment;
import com.paypal.base.rest.APIContext;
import com.paypal.api.openidconnect.Userinfo;
import com.paypal.base.rest.PayPalRESTException;
import com.paypal.api.payments.CreditCard;
import com.paypal.api.payments.Transaction;

import com.paypal.api.payments.Currency;
import com.paypal.api.payments.Payout;
import com.paypal.api.payments.PayoutBatch;
import com.paypal.api.payments.PayoutItem;
import com.paypal.api.payments.PayoutSenderBatchHeader;

public class Paypal_Test {
	
	//these are the ClientId and the Secrets from SandBox mgunn001-facilitator1@odu.edu
	private String clientId = "Ac8sGOBAyL663MiTvaJARqwXo0pfr-wfeMJdvHHDANlykFIQDmQdRILpgx0AX_lrAdXnNzswDF0cwucR";
	private String clientSecret = "EOEe5xNv-IgFYcIY365eVqnsdq4xkpt9Anu6pJJuVHwx_PWS4fKg-Jp_dLOJzEgxfMd1FOcossTNqr0a";
	private APIContext apiContext = new APIContext(clientId, clientSecret, "sandbox");

	//to ge the userinfo on the card
	public void getUserInfo() throws Exception {
		
		Userinfo userinfo = Userinfo.getUserinfo(apiContext);
		System.out.println(userinfo);	
	}
	
	
	// this method for adding a new credit card
	public void getAddCreditCard() throws Exception {	
	
		Userinfo userinfo = Userinfo.getUserinfo(apiContext);
		System.out.println(userinfo);
		CreditCard card = new CreditCard()
                .setType("visa")
                .setNumber("4417119669820331")
                .setExpireMonth(11)
                .setExpireYear(2019)
                .setCvv2(012)
                .setFirstName("Joe")
                .setLastName("Shopper");
        try {
            card.create(apiContext);
            System.out.println(card);
        } catch (PayPalRESTException e) {
            System.err.println(e.getDetails());
        }

	}
	
	
	// this method takes care of future payment functionality (Client to Business)
	public  void makeFuturePayment() {		
		try {
			// Authorization Code and Co-relationID retrieved from Mobile SDK.
			// this authorizationCode is extracted after the user verify the login
			String authorizationCode = "C101.Rya9US0s60jg-hOTMNFRTjDfbePYv3W_YjDJ49BVI6YJY80HvjL1C6apK8h3IIas.ZWOGll_Ju62T9SXRSRFHZVwZESK";
			String correlationId = "123456123";
			
			// Fetch the long lived refresh token from authorization code.
			String refreshToken = FuturePayment.fetchRefreshToken(apiContext, authorizationCode);
			// Store the refresh token in long term storage for future use.

			// Set the refresh token to context to make future payments of
			// pre-consented customer.
			apiContext.setRefreshToken(refreshToken);
			
			// Create Payment Object
			Payer payer = new Payer();
			payer.setPaymentMethod("paypal");
			
			Amount amount = new Amount();
			amount.setTotal("0.17");
			amount.setCurrency("USD");
			
			Transaction transaction = new Transaction();
			transaction.setAmount(amount);
			transaction.setDescription("tesing the future payments paypal sandbox.");
			
			List<Transaction> transactions = new ArrayList<Transaction>();
			transactions.add(transaction);

			FuturePayment futurePayment = new FuturePayment();
			futurePayment.setIntent("authorize");
			futurePayment.setPayer(payer);
			futurePayment.setTransactions(transactions);

			Payment createdPayment = futurePayment.create(apiContext, correlationId);
			System.out.println(createdPayment.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(Payment.getLastRequest());
			System.out.println(Payment.getLastResponse());
		}
	}
	

	
	// method to create a payment and set all the attributes to it (Client to Business)
	public void creatPayment(){
		
			Payer payer = new Payer();
			payer.setPaymentMethod("paypal");
			// Set redirect URLs
			RedirectUrls redirectUrls = new RedirectUrls();
			redirectUrls.setCancelUrl("http://localhost:8080/JaxRSDemo/resources/requestcancelled");
			redirectUrls.setReturnUrl("http://localhost:8080/JaxRSDemo/resources/requestunderprocess");
	
			// Set payment details
			Details details = new Details();
			details.setShipping("1");
			details.setSubtotal("5");
			details.setTax("1");
	
			// Payment amount
			Amount amount = new Amount();
			amount.setCurrency("USD");
			// Total must be equal to sum of shipping, tax and subtotal.
			amount.setTotal("7");
			amount.setDetails(details);
	
			// Transaction information
			Transaction transaction = new Transaction();
			transaction.setAmount(amount);
			transaction.setDescription("This is a test transaction through Paypal.");
	
			// Add transaction to a list
			List<Transaction> transactions = new ArrayList<Transaction>();
			transactions.add(transaction);
	
			// Add payment details
			Payment payment = new Payment();
			payment.setIntent("sale");
			payment.setPayer(payer);
			payment.setRedirectUrls(redirectUrls);
			payment.setTransactions(transactions);
			
			try{
				
			 Payment createdPayment = payment.create(apiContext);
			 Iterator<Links> links = createdPayment.getLinks().iterator();
			  while (links.hasNext()) {
			    Links link = links.next();
			    if (link.getRel().equalsIgnoreCase("approval_url")) {
			    	System.out.println("Inside the approve URL");
			    	// this is the link that gets the credentials giving screen
			      // REDIRECT USER TO link.getHref()
			    }
			  }
			} catch (PayPalRESTException e) {
			    System.err.println(e.getDetails());
			}

	}
	
	// this is when the transaction gets completed (Client to Business), this method has to be called from the processing url that you set with the Payment Object
	public void completePayment(){
		Payment payment = new Payment();
		//payment.setId(req.getParameter("paymentId"));

		PaymentExecution paymentExecution = new PaymentExecution();
		//paymentExecution.setPayerId(req.getParameter("PayerID"));
		try {
		  Payment createdPayment = payment.execute(apiContext, paymentExecution);
		  System.out.println(createdPayment);
		} catch (PayPalRESTException e) {
		  System.err.println(e.getDetails());
		}
	}
	
	// this is method to transfer the amount from a (Business to Client)
	public void singlePayout(){
		
		// ###Payout
				// A resource representing a payout
				Payout payout = new Payout();

				PayoutSenderBatchHeader senderBatchHeader = new PayoutSenderBatchHeader();

				// ### NOTE:
				// You can prevent duplicate batches from being processed. If you
				// specify a `sender_batch_id` that was used in the last 30 days, the
				// batch will not be processed. For items, you can specify a
				// `sender_item_id`. If the value for the `sender_item_id` is a
				// duplicate of a payout item that was processed in the last 30 days,
				// the item will not be processed.
				// #### Batch Header Instance
				Random random = new Random();
				senderBatchHeader.setSenderBatchId(
						new Double(random.nextDouble()).toString()).setEmailSubject(
						"You have a Payout!");

				// ### Currency
				Currency amount = new Currency();
				amount.setValue("1.00").setCurrency("USD");

				// #### Sender Item
				// Please note that if you are using single payout with sync mode, you
				// can only pass one Item in the request
				PayoutItem senderItem = new PayoutItem();
				senderItem.setRecipientType("Email")
						.setNote("Thanks for your patronage")
						.setReceiver("mgunn001-buyer@odu.edu")
						.setSenderItemId("201404324234").setAmount(amount);

				List<PayoutItem> items = new ArrayList<PayoutItem>();
				items.add(senderItem);

				payout.setSenderBatchHeader(senderBatchHeader).setItems(items);

				PayoutBatch batch = null;
				try {

					// ### Api Context
					// Pass in a `ApiContext` object to authenticate
					// the call and to send a unique request id
					// (that ensures idempotency). The SDK generates
					// a request id if you do not pass one explicitly.
					//APIContext apiContext = new APIContext(clientId, clientSecret, mode);
				
					// ###Create Payout Synchronous
					batch = payout.createSynchronous(apiContext);

					/*LOGGER.info("Payout Batch With ID: "
							+ batch.getBatchHeader().getPayoutBatchId());*/
					
					/*ResultPrinter.addResult(req, resp,
							"Created Single Synchronous Payout",
							Payout.getLastRequest(), Payout.getLastResponse(), null);*/
					System.out.println("lastRequest:" + Payout.getLastRequest());
					System.out.println("lastRequest:" + Payout.getLastResponse());
				} catch (PayPalRESTException e) {
					/*ResultPrinter.addResult(req, resp,
							"Created Single Synchronous Payout",
							Payout.getLastRequest(), null, e.getMessage());*/
				}

				System.out.println("lastRequest:" + batch);
				//return batch;
	}
	
	
	
	
}
