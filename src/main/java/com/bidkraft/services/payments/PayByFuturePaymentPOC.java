package com.bidkraft.services.payments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.FutureTranxAuthorization;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.responses.FPInitiationResponse;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.ConfigFileReader;
import com.paypal.api.payments.Currency;
import com.paypal.api.payments.Payout;
import com.paypal.api.payments.PayoutBatch;
import com.paypal.api.payments.PayoutItem;
import com.paypal.api.payments.PayoutSenderBatchHeader;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;


/**
 * @author mgunnam
 * 
 * this method receives the amount, clientmetadataid, and process the payment from client to bidkraft
 *
 */
@Component
@SuppressWarnings("unchecked")
public class PayByFuturePaymentPOC implements KraftService<FPInitiationResponse>{

	@Override
	public FPInitiationResponse service(KraftRequest request) throws KraftException {
		
		FPInitiationResponse dummyFPRespObj = new FPInitiationResponse();		
		String clientMetaDataId = "";
		if (request.getEntities().containsKey(ServiceKeys.PAYBYFUTUREPAYMENT)) {
		
			Map<String, String> dummyFuturePaymentIPMap = (Map<String, String>) request.getEntities().get(ServiceKeys.PAYBYFUTUREPAYMENT);
			System.out.println("This is the service to process the Future Payment Paypal :PayByFuturePayment");
			Long userId = Long.parseLong(dummyFuturePaymentIPMap.get("userid"));
			dummyFPRespObj.setUserId(userId);
			dummyFPRespObj.setAmount(Double.parseDouble(dummyFuturePaymentIPMap.get("amount")));
			clientMetaDataId = dummyFuturePaymentIPMap.get("clientmetadataid");
			
			System.out.println("\n**********\nClientMetaDataId: "+ clientMetaDataId);
			FutureTranxAuthorization futureTranxAutObj = new FutureTranxAuthorization();
			futureTranxAutObj = getFutureTranxAutObjByUserId(userId);

		
			String accessToken = "";
			try{
				//refreshed access tokens are get here, using which the payment
				accessToken = getRefreshedAccessToken(futureTranxAutObj.getRefreshToken());
								
				// call this method to initiate the payment
				Boolean isApproved =  createFuturePaymentThroughHTTP(accessToken,clientMetaDataId,dummyFPRespObj.getAmount());	
				if(isApproved){	
					 
					 // make a payout call here for demo purpose only
					 Boolean isPayoutComplete = singlePayOutForDemo(dummyFPRespObj.getAmount());
					if(isPayoutComplete){
						 dummyFPRespObj.setPaymentStatus("SUCCESS");
					 }else{
						 dummyFPRespObj.setPaymentStatus("FAILED");
					 }
							 
				 }else{
					 dummyFPRespObj.setPaymentStatus("FAILED");
				 }

			}catch (Exception e) {
				// TODO: handle exception
				KraftException ke = new KraftException(e.getMessage());
				throw ke;
			}

		}
		return dummyFPRespObj;
	}
	
	// this method gets the refresh token (long living token) from DB for an user
	@SuppressWarnings("unchecked")
	public FutureTranxAuthorization getFutureTranxAutObjByUserId(Long userid) {
		List<FutureTranxAuthorization> futureTranxAuthorization;
		Session session = HibernateUtil.getSessionFactory().openSession();

		Query query = session.createQuery("from FutureTranxAuthorization where user_id= :userid ORDER BY id DESC");
		query.setParameter("userid", userid);

		futureTranxAuthorization = query.list();
		return futureTranxAuthorization.get(0);
	}
	
	
	// this method is for getting the access token which inturn uses the long lived refresh token 
	@SuppressWarnings("deprecation")
	public String getRefreshedAccessToken(String refreshToken) throws Exception {	
		
		String url = "https://api.sandbox.paypal.com/v1/oauth2/token";
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		
		String authStrInHeader = PaymentPaypalKeys.merchantId+":"+PaymentPaypalKeys.merchantSecret;
		String base64AuthStrInHeader = Base64.getUrlEncoder().encodeToString(authStrInHeader.getBytes("utf-8"));	
		
		// add header	
		post.setHeader("Content-Type", "application/x-www-form-urlencoded");
		post.setHeader("Authorization", "BASIC "+base64AuthStrInHeader);
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
		urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		
		
		System.out.println("\n*** Refreshing the Access Token**** Sending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " +
                                    response.getStatusLine().getStatusCode());
		
		String json_string = readResponseContent(response);
		JSONObject jobj = new JSONObject(json_string);
		String accessToken = jobj.getString("access_token");
		System.out.println("AccessToken which is refreshed:"+ accessToken);
		return accessToken;
	}

	
	// this method is for the Creating FuturePayments through the Http calls
	@SuppressWarnings("deprecation")
	public Boolean createFuturePaymentThroughHTTP(String accessToken,String clientMetaDataId,Double amount) throws Exception {	
		
		String url = "https://api.sandbox.paypal.com/v1/payments/payment";
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		
		// add header	
		post.setHeader("Content-Type", "application/json");
		post.setHeader("PayPal-Client-Metadata-Id", clientMetaDataId);
		post.setHeader("Authorization", "Bearer "+accessToken);
		
		
		//create the json object to set to the request
		String inputReqObjStr = "{\"intent\":\"authorize\",\"payer\":{\"payment_method\":\"paypal\"},\"transactions\":[{\"amount\":{\"currency\":\"USD\",\"total\":\""+amount+"\"},\"description\":\"future of sauces\"}]}";
		
		StringEntity inputReqObjEntity = new StringEntity(inputReqObjStr);			
		post.setEntity(inputReqObjEntity);
		HttpResponse response = client.execute(post);

		System.out.println("\n*** Creating the Payment**** Sending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " +
                                    response.getStatusLine().getStatusCode());
		
		String json_string = readResponseContent(response);
		JSONObject jobj = new JSONObject(json_string);

		// this is just for the demo purpose, usually the id of this created transaction should be stored in DB
		if(jobj.getString("state").equals("approved")){
			try{

				// call the completeFuturePaymentThroughHTTP here to capture the Amount 				
				//this captureLink has to be set by iterating through the Json returned from create Payment method
				
				 jobj = new JSONObject(json_string);
				// call the completeFuturePaymentThroughHTTP here to capture the Amount 				
				//this captureLink has to be set by iterating through the Json returned from create Payment method
				JSONArray responseLinksObjList =  jobj.getJSONArray("transactions");	
				
				JSONArray relatedResourcesList = ((JSONObject)responseLinksObjList.get(0)).getJSONArray("related_resources");			

				JSONObject authorizationObj= ((JSONObject) relatedResourcesList.get(0)).getJSONObject("authorization");	
				
				JSONArray LinksList = authorizationObj.getJSONArray("links");
				
				String captureLink ="";
				for (int i = 0; i < LinksList.length(); i++) {					
					JSONObject jsonObj = LinksList.getJSONObject(i);
					  if(jsonObj.getString("rel").equals("capture") ){
						  captureLink= jsonObj.getString("href");
						  break;
					  }					  
				}
		
				System.out.println("\n**************\nCaptureLink for capturing the payment  : " +captureLink);
				
				Boolean isCompleted = completeFuturePaymentThroughHTTP(accessToken,clientMetaDataId,amount,captureLink);
				return isCompleted;
				
			}catch (Exception e) {
				// TODO: handle exception
				KraftException ke = new KraftException(e.getMessage());
				throw ke;
			}
		}else{
			return false;
		}
	
	}
	
	// this method is for the Capturing FuturePayments through the Http calls
	@SuppressWarnings("deprecation")
	public Boolean completeFuturePaymentThroughHTTP(String accessToken,String clientMetaDataId,Double amount,String captureLink) throws Exception {	
		
		String url = captureLink;
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);		
		// add header	
		post.setHeader("Content-Type", "application/json");
		post.setHeader("PayPal-Client-Metadata-Id", clientMetaDataId);
		post.setHeader("Authorization", "Bearer "+accessToken);
		
		
		//create the json object to set to the request
		String inputReqObjStr = "{\"amount\":{\"currency\":\"USD\",\"total\":\""+ Math.round(amount)+"\"},\"is_final_capture\":true}";
		
		StringEntity inputReqObjStrEntity = new StringEntity(inputReqObjStr);			
		post.setEntity(inputReqObjStrEntity);
		HttpResponse response = client.execute(post);

		System.out.println("\n*** Capturing the Payment **** Sending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " +
                                    response.getStatusLine().getStatusCode());

		String json_string = readResponseContent(response);
		JSONObject jobj = new JSONObject(json_string);
		
		// this is just for the demo purpose, usually the id of this created transaction should be stored in DB
		if(jobj.getString("state").equals("completed")){
			return true;
		}else{
			return false;
		}
	
	}
	
	
	// this method helps in reading the response and making a json object out of it
	public String readResponseContent( HttpResponse response ) throws UnsupportedOperationException, IOException{
		BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
		
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return result.toString();
	}
	
	
	//this following  method is for demo purpose only, cuts the bidkraft's share and sends the amount to service provider
	public Boolean singlePayOutForDemo(Double amount) throws PayPalRESTException, IOException{
		Payout payout = new Payout();
		PayoutBatch payoutBatch = null;
		PayoutSenderBatchHeader senderBatchHeader = new PayoutSenderBatchHeader();

		// ### NOTE:
		// You can prevent duplicate batches from being processed. If you
		// specify a `sender_batch_id` that was used in the last 30 days, the
		// batch will not be processed. For items, you can specify a
		// `sender_item_id`. If the value for the `sender_item_id` is a
		// duplicate of a payout item that was processed in the last 30 days,
		// the item will not be processed.
		// #### Batch Header Instance
		Random random = new Random();
		senderBatchHeader.setSenderBatchId(
				new Double(random.nextDouble()).toString()).setEmailSubject(
				"You have a Payout!");

		// ### Currency
		//*************** the following code to read the bidkraft cut percentage from a properties file **************
		ConfigFileReader cfReader = new ConfigFileReader();
		Double bidKraftCutPer = (Double) cfReader.getBidKraftCut();
		Double fees = (amount * bidKraftCutPer)/100;								
		//double fees = (bidAmount * 10)/100;			
		Double amountToPayout = amount - fees;
		
		String amountToPayoutStr =  Double.toString(amountToPayout);
		System.out.println("Amoun To Payout String: " +amountToPayoutStr);
		Currency currencyAmt = new Currency();
		currencyAmt.setValue(amountToPayoutStr).setCurrency("USD");
		//currencyAmt.setValue(totalAmount.toString()).setCurrency("USD");
		
		
		// #### Sender Item
		// Please note that if you are using single payout with sync mode, you
		// can only pass one Item in the request
		PayoutItem senderItem = new PayoutItem();
		senderItem.setRecipientType("Email")
				.setNote("Thanks for your patronage")	// this attributes have to be set dynamically
				.setReceiver("mgunn001-buyer1@odu.edu")
				.setSenderItemId("201404324234").setAmount(currencyAmt);

		List<PayoutItem> items = new ArrayList<PayoutItem>();
		items.add(senderItem);

		payout.setSenderBatchHeader(senderBatchHeader).setItems(items);

		// ### Api Context			
		APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");
			
		// ###Create Payout Synchronous
		payoutBatch = payout.createSynchronous(apiContext);
		System.out.println("lastRequest:" + Payout.getLastRequest());
		System.out.println("lastRequest:" + Payout.getLastResponse());
		System.out.println("lastRequest:" + payoutBatch);
		
		/* Have to check the response and return true or false accordingly
		 * if(){
			
		}else{
			
		}*/
				
		return true;
	}
	
	
}
