package com.bidkraft.services.payments;

import org.springframework.stereotype.Component;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.services.KraftService;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Component
@SuppressWarnings("unchecked")
public class CompletePaypalPayment implements KraftService<Payment>{

	@Override
	public Payment service(KraftRequest request) throws KraftException {
		Payment payment = new Payment();
		//payment.setId(req.getParameter("paymentId"));
		Payment completedPayment = null;
		PaymentExecution paymentExecution = new PaymentExecution();
		//paymentExecution.setPayerId(req.getParameter("PayerID"));
		if (request.getEntities().containsKey(ServiceKeys.COMPLETEPAYPALPAYMENT)) {
			try {
				// ### Api Context			
			  APIContext apiContext = new APIContext(PaymentPaypalKeys.merchantId, PaymentPaypalKeys.merchantSecret, "sandbox");
			  completedPayment = payment.execute(apiContext, paymentExecution);
			  System.out.println(completedPayment);
			} catch (PayPalRESTException e) {
			  System.err.println(e.getDetails());
			}
		}
		return completedPayment;
	}

}
