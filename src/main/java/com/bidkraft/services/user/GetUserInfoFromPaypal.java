package com.bidkraft.services.user;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.PriceLine;
import com.bidkraft.entities.User;
import com.bidkraft.enums.PriceLineTaggedTo;
import com.bidkraft.enums.PriceLineType;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.paypalutil.PaypalPaymentUtil;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.HttpResponseReader;

@Component
@SuppressWarnings("unchecked")
public class GetUserInfoFromPaypal implements KraftService<User> {

	@Override
	public User service(KraftRequest request) throws KraftException {
	
		User userObj = new User();
		if (request.getEntities().containsKey(ServiceKeys.GETUSERINFOFROMPAYPAL)) {
			Map<String, String> getUserDetailAuthMap = (Map<String, String>) request.getEntities().get(ServiceKeys.GETUSERINFOFROMPAYPAL);
			System.out.println("This is the service to get the User Auth key from the Mobile client and get the userinfo:");			
			
			JSONObject accessRefreshTokenJObj = new JSONObject();
			try{
				
				PaypalPaymentUtil paypalPaymentUtil = new PaypalPaymentUtil();	
				//Here the code gets the Json Object which has the long lived Refresh Token and Access token
				accessRefreshTokenJObj =paypalPaymentUtil.getAccessAndRefreshToken(getUserDetailAuthMap.get("authkey"));	
				
				// call the method getIdentityByAccessToken to get the identity
				HttpResponse response =  getIdentityByAccessToken(accessRefreshTokenJObj.getString("access_token"));
				HttpResponseReader httpResponseReader = new HttpResponseReader();
				String httpRespStr =  httpResponseReader.readHttpResponseContent(response);
				
				// the following object would have structure of something like the below 
					/*{
					  "user_id": "https://www.paypal.com/webapps/auth/server/64ghr894040044",
					  "name": "Peter Pepper",
					  "given_name": "Peter",
					  "family_name": "Pepper",
					  "email": "ppuser@example.com"
					}*/
				
				
				userObj = addUserIfNotExisting(httpRespStr);
				
			}catch (Exception e) {
			
				// TODO: handle exception
				KraftException ke = new KraftException(e.getMessage());
				throw ke;
			}
		
		}
		
		return userObj;
	}
	
	

	// this method is to get the Paypal User Identity from the accessToken
	@SuppressWarnings("deprecation")
	public HttpResponse getIdentityByAccessToken(String accessToken) throws ClientProtocolException, IOException{	
		
		// this is the url for the Identity openid connect
		String url = "https://api.sandbox.paypal.com/v1/identity/openidconnect/userinfo/?schema=openid";
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpGet getReq = new HttpGet(url);	
		
		// add header	
		getReq.setHeader("Content-Type", "application/json");
		getReq.setHeader("Authorization", "Bearer "+accessToken);
		HttpResponse response = client.execute(getReq);

		System.out.println("\n*** Getting the User Identity **** Sending 'Get' request to URL : " + url);
		System.out.println("Response Code : " +
                                    response.getStatusLine().getStatusCode());
		return response;
	}
	
	
	
	// this method is to get the Latitude and Longitude from the Address string
	@SuppressWarnings("deprecation")
	public JSONObject getLatLongFromAddress(JSONObject address) throws JSONException, ClientProtocolException, IOException{	
		
		// this one is got from my EmailID : maheedhargunnam@gmail.com
		String APIKey = "AIzaSyA28n5arlMhJ8xjhVM7ReNQZcyXgP7IepU";
		
		//sample - http://maps.googleapis.com/maps/api/geocode/json?address=1049+W+49th%20street,+Norfolk,+Virginia&key=YOUR_API_KEY
		// this is the url for the Identity openid connect
		/*String url = "https://maps.googleapis.com/maps/api/geocode/json?address=";
			url+=address.getString("street_address")+", ";
			url+=address.getString("locality")+", ";
			url+=address.getString("region")+", ";
			url+=address.getString("postal_code");
			url+="&key="+APIKey;*/
		
		String url = "https://maps.googleapis.com/maps/api/geocode/json?address=";
		url+= URLEncoder.encode(address.getString("street_address")+", ","UTF-8");
		url+= URLEncoder.encode(address.getString("locality")+", ","UTF-8");
		url+= URLEncoder.encode(address.getString("region")+", ","UTF-8");
		url+= URLEncoder.encode(address.getString("postal_code"),"UTF-8");
		url+="&key="+APIKey;

		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpGet getReq = new HttpGet(url);	
		
		HttpResponse response = client.execute(getReq);

		System.out.println("\n*** Getting the Geo Location (Lat, Long) From Address **** Sending 'Get' request to URL : " + url);
		System.out.println("Response Code : " +response.getStatusLine().getStatusCode());
		HttpResponseReader httpResponseReader = new HttpResponseReader();
		String httpRespStr =  httpResponseReader.readHttpResponseContent(response);
		JSONObject latLongObj = new JSONObject(httpRespStr);
		return latLongObj;
	}
	
	
	
	// this method is to get the Latitude and Longitude from the Address string
		@SuppressWarnings("deprecation")
	public HttpResponse getAddressFromLatLong(double lat, double longitude) throws JSONException, ClientProtocolException, IOException{	
				
		// this one is got from my EmailID : maheedhargunnam@gmail.com
		String APIKey = "AIzaSyA28n5arlMhJ8xjhVM7ReNQZcyXgP7IepU";
		
		//sample - https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY

		// this is the url for the Identity openid connect
		String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
			url+=lat+", ";
			url+=longitude;			
			url+="&key="+APIKey;
		
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpGet getReq = new HttpGet(url);	
		
		HttpResponse response = client.execute(getReq);

		System.out.println("\n*** Getting the Geo Location (Address) From Latlong **** Sending 'Get' request to URL : " + url);
		System.out.println("Response Code : " +response.getStatusLine().getStatusCode());
		return response;
	}	
		
	
	// to check to see if this user is already logged in to Bidkraft through paypal, or the registration(Adding user to our system) takes place here
	@SuppressWarnings("deprecation")
	public User addUserIfNotExisting(String httpRespStr) throws KraftException, JSONException{
		
		JSONObject paypalIdentityJObj = new JSONObject(httpRespStr);
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transcation = session.beginTransaction();
		User userObj = new User();
		try{
			
			// these two are changed cause a paypal user now is uniquely identified by user_id
			//String emailIdFromIdentity = paypalIdentityJObj.getString("email");
			//Query getUserIfExistQuery = session.createQuery("from User where email = :emailId");
			
			String user_idFromIdentity = paypalIdentityJObj.getString("user_id");
			Query getUserIfExistQuery = session.createQuery("from User where paypalUserURL = :userURL");
			getUserIfExistQuery.setParameter("userURL", user_idFromIdentity);
			
			List<User> usersList = getUserIfExistQuery.list();
	
			if(usersList.size() > 0) {				
				userObj = usersList.get(0);								
			}else{
				
				if(paypalIdentityJObj.has("address") && paypalIdentityJObj.has("email")){				
					// if none exist in the DB with this emailId, Create an user with these detail, may also have to capture the Phone number etc
					JSONObject latLongObj = getLatLongFromAddress(paypalIdentityJObj.getJSONObject("address"));
					double lat = (double)((JSONObject)latLongObj.getJSONArray("results").get(0)).getJSONObject("geometry").getJSONObject("location").get("lat");
					double lng = (double)((JSONObject)latLongObj.getJSONArray("results").get(0)).getJSONObject("geometry").getJSONObject("location").get("lng");
					userObj.setEmail(paypalIdentityJObj.getString("email"));
					userObj.setPaypalRegisteredEmail(paypalIdentityJObj.getString("email"));
					
					if(paypalIdentityJObj.has("given_name")){
						userObj.setFirstName(paypalIdentityJObj.getString("given_name"));
					}
					if(paypalIdentityJObj.has("family_name")){
						userObj.setLastName(paypalIdentityJObj.getString("family_name"));
					}
					if(paypalIdentityJObj.has("phone_number")){
						userObj.setPhone(paypalIdentityJObj.getString("phone_number"));	
					}
		
					//userObj.setFirstName(paypalIdentityJObj.getString("given_name"));
					//userObj.setLastName(paypalIdentityJObj.getString("family_name"));
					//userObj.setPhone(paypalIdentityJObj.getString("phone_number"));	
					
					
					userObj.setPaypalUserURL(user_idFromIdentity);
					
					
					userObj.setPaypaIdentityResponse(Hibernate.createBlob(httpRespStr.getBytes()));
					userObj.setPassword("dummydummy");	
					//these two have to be set by some other external API, for now setting it to the following hardcoded values	
					userObj.setLatitude(lat);
					userObj.setLongitude(lng);				
					userObj.setBgcstatus(ServiceKeys.BGC_INTIAL);
					userObj.setRating(Double.parseDouble("5"));		
					userObj.setIsPayPalAuth("no");
					session.save(userObj);
					transcation.commit();
				}else{
					KraftException ke = new KraftException("User hasn't given Consent to access his Information.");
					throw ke;
				}
			}

		}catch(Exception e) {
			session.getTransaction().rollback();
			KraftException ke = new KraftException(e.getMessage());
			throw ke;
		}
		session.flush();
		return userObj;
	}
	
}
