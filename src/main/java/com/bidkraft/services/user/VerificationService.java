package com.bidkraft.services.user;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Verification;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class VerificationService implements KraftService<String> {

	@Override
	public String service(KraftRequest request) throws KraftException {
		
		String msg="";
				
		if (request.getEntities().containsKey(ServiceKeys.VERIFICATION)) {
			Map<String, String> usermap = (Map<String, String>) request
					.getEntities().get(ServiceKeys.VERIFICATION);

			Session session = HibernateUtil.getSessionFactory().openSession();

			Query query = session
					.createQuery("from Verification where userid= :userid and  tokenid= :token");
			
			query.setParameter("userid", Long.parseLong(usermap.get("userid")));
			query.setParameter("token",usermap.get("tokenid"));

			List<Verification> verification = query.list();
			if (verification.size() == 0) {
				throw new KraftException("Token Id not available");
			}
			else
			{
				msg=verification.get(0).getToken();
			}
		}
		return msg;
	}
}
