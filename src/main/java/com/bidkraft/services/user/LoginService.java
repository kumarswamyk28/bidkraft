package com.bidkraft.services.user;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.User;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class LoginService implements KraftService<User> {

	@SuppressWarnings("unchecked")
	@Override
	public User service(KraftRequest request) throws KraftException {

		User user = new User();

		if (request.getEntities().containsKey(ServiceKeys.LOGIN)) {
			Map<String, String> usermap = (Map<String, String>) request.getEntities().get(ServiceKeys.LOGIN);

			System.out.println("In Login Service:" + request.getEntities().get(ServiceKeys.LOGIN));

			System.out.println("User info" + usermap.get("password"));

			Session session = HibernateUtil.getSessionFactory().openSession();

			Query query = session.createQuery("from User where email= :email and  password= :pwd");
			query.setParameter("email", usermap.get("email"));
			query.setParameter("pwd", usermap.get("password"));

			List<User> users = query.list();
			if (users.size() == 0) {
				throw new KraftException("Userid or password is wrong");
			} else {
				user = users.get(0);
			}
		}
		return user;
	}

}
