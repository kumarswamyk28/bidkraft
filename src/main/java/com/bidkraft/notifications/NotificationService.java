package com.bidkraft.notifications;

import com.bidkraft.exception.KraftException;

/**
 * @author kkallepalli
 *
 * @param <T>
 */
public interface NotificationService {
	
	public void sendNotification(Object response) throws KraftException;

}
