package com.bidkraft.notifications;



import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.bidkraft.entities.Bid;
import com.bidkraft.exception.KraftException;


/**
 * @author kkallepalli
 *
 */
public class CreateBidNotificationService implements NotificationService {

	@Override
	public void sendNotification(Object response) throws KraftException {
		Bid bid=(Bid)response;
		
		

			
			JSONObject obj = new JSONObject();
			
			try {
				obj.put("message", "Created Bid");
				obj.put("age", new Integer(100));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			AndroidContent content = new AndroidContent();
			content.createRegId("fOeg_QpUGtQ:APA91bHfjZpnp4Y_mHPKGrLCeF4tsOsC02ke6pIOvJTs1oZDSbfeZCk7ioDiOXLGOzS_uu0mliMO5XF6fNyn6wGUVz90C3yqBYD2TDq9RJwoYZZb1KWWlwghkwVOy3OZmmXO__Rud-CP");
			content.createData("data",obj);
	        Post2GCM.post(content);
			
		
	}

}
