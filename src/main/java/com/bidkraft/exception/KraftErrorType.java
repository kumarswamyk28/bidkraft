package com.bidkraft.exception;

public enum KraftErrorType {
	SYSTEM(1, "System"),
	INPUT(2, "Input"),
	DATABASE(3, "Database"),
	ES(4, "Elasticsearch");
		
	private int id;
	private String name;
	
	private KraftErrorType(int id, String name) {
		this.id = id;
		this.name();
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

}
