package com.bidkraft.exception;

public class KraftException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private KraftError error;
	private String errorMessage;
	
	public KraftException(String message) {
		super(message);
		this.errorMessage = message;
		this.error = KraftError.UNKNOWN;
	}
	
	public KraftException(KraftError error, String message)
	{
		super(message);
		this.error=error;
	}
	

	public KraftError getError() {
		return error;
	}

	public void setError(KraftError error) {
		this.error = error;
	}
	
	public String getErrorMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append(errorMessage);
		
		//TODO : Get the complete error message from kraft error.
		//sb.append(c)
		
		return sb.toString();
	}


}
