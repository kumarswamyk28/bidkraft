package com.bidkraft.enums;

public enum PriceLineTaggedTo {
	
	CUSTOMER(1, "CUSTOMER"),
	VENDOR(2, "VENDOR");

	private int id;
	private String name;
	
	private PriceLineTaggedTo(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
