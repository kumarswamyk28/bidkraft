package com.bidkraft.enums;

public enum DistributionStatus {
	SUCCESS(1, "SUCCESS"),
	APPROVED(2, "APPROVED"),
	NOTSUCCESS(3,"NOSUCCESS"), // have to see the other status 
	PENDING(4,"PENDING");
	
	private int id;
	private String name;
	
	private DistributionStatus(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
}
