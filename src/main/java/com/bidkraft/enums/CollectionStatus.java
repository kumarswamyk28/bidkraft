package com.bidkraft.enums;

public enum CollectionStatus {
	INITIATED(1, "initiated"),
	APPROVED(2, "approved"),
	COMPLETED(3, "completed");
	
	private int id;
	private String name;
	
	private CollectionStatus(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

}
