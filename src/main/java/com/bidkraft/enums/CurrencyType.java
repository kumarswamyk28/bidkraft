package com.bidkraft.enums;

public enum CurrencyType {
	USD(1, "USD");
	
	
	private int id;
	private String name;
	
	private CurrencyType(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
}
