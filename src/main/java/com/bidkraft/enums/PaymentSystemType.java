/**
 * Written by: Mahee
 * Date: March/14/2017
 */


package com.bidkraft.enums;

public enum PaymentSystemType {
	
	PAYPAL(1, "PAYPAL");
	
	private int id;
	private String name;

	private PaymentSystemType(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
