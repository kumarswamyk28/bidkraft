package com.bidkraft.enums;

public enum UserPayPalAuthStatus {
	
	YES(1, "yes"),
	NO(2, "no");

	private int id;
	private String name;
	
	private UserPayPalAuthStatus(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
}
