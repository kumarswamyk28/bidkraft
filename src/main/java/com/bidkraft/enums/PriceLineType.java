package com.bidkraft.enums;

public enum PriceLineType {
	TOTAL_AMOUNT(1, "TOTAL_AMOUNT"),
	COUPON(2, "COUPON"),
	FEES(3, "FEES"),
	ITEM_COST(4, "ITEM_COST");
	
	private int id;
	private String name;
	
	private PriceLineType(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}


}
