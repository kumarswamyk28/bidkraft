package com.bidkraft.paypalutil;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;

import com.bidkraft.constants.PaymentPaypalKeys;
import com.bidkraft.entities.FutureTranxAuthorization;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.util.HttpResponseReader;

public class PaypalPaymentUtil {
	
	
	// this method gets the refresh token (long living token) from DB for an user
	@SuppressWarnings("unchecked")
	public FutureTranxAuthorization getFutureTranxAutObjByUserId(Long userid) {
		List<FutureTranxAuthorization> futureTranxAuthorization;
		Session session = HibernateUtil.getSessionFactory().openSession();

		Query query = session.createQuery("from FutureTranxAuthorization where user_id= :userid ORDER BY id DESC");
		query.setParameter("userid", userid);

		futureTranxAuthorization = query.list();
		return futureTranxAuthorization.get(0);
	}
	
	
	//this method is to get both the Access and Refresh Token on the very first user consent authentication screen soon after registration
	@SuppressWarnings("deprecation")
	public JSONObject getAccessAndRefreshToken(String initalAuthKey) throws Exception {
		String url = "https://api.sandbox.paypal.com/v1/oauth2/token";
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		String authStrInHeader = PaymentPaypalKeys.merchantId+":"+PaymentPaypalKeys.merchantSecret;
		String base64AuthStrInHeader = Base64.getUrlEncoder().encodeToString(authStrInHeader.getBytes("utf-8"));
		
		// add header	
		post.setHeader("Content-Type", "application/x-www-form-urlencoded");	
		post.setHeader("Authorization", "BASIC "+base64AuthStrInHeader);
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("grant_type", "authorization_code"));
		urlParameters.add(new BasicNameValuePair("response_type", "token"));
		urlParameters.add(new BasicNameValuePair("redirect_uri", "urn:ietf:wg:oauth:2.0:oob"));
		urlParameters.add(new BasicNameValuePair("code", initalAuthKey));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);
		
		
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

		HttpResponseReader httpResponseReader = new HttpResponseReader();
		String httpRespStr =  httpResponseReader.readHttpResponseContent(response);
		JSONObject jobj = new JSONObject(httpRespStr);
		return jobj;
	}
	
	
	// this method is for getting the access token which inturn uses the long lived refresh token 
	@SuppressWarnings("deprecation")
	public JSONObject getRefreshedAccessTokenObj(String refreshToken) throws Exception {	
		
		String url = "https://api.sandbox.paypal.com/v1/oauth2/token";
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		
		String authStrInHeader = PaymentPaypalKeys.merchantId+":"+PaymentPaypalKeys.merchantSecret;
		String base64AuthStrInHeader = Base64.getUrlEncoder().encodeToString(authStrInHeader.getBytes("utf-8"));	
		
		// add header	
		post.setHeader("Content-Type", "application/x-www-form-urlencoded");
		post.setHeader("Authorization", "BASIC "+base64AuthStrInHeader);
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
		urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		
		
		System.out.println("\n*** Refreshing the Access Token**** Sending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " +
                                    response.getStatusLine().getStatusCode());
		
		HttpResponseReader httpResponseReader = new HttpResponseReader();
		String json_string =  httpResponseReader.readHttpResponseContent(response);
		JSONObject jobj = new JSONObject(json_string);
		return jobj;
	}
	
	

}
