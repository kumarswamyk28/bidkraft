package com.bidkraft.entities;

import java.sql.Blob;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.codehaus.jackson.annotate.JsonIgnore;

public class DistributionPayoutResponse {

	private long distributionPayoutResponseID;
	private long distributionId; //this is the foriegn key with the Distribution class ID
	private long senderBatchId;
	private Timestamp createdDateTime;
	private String payoutResponseBatchId; //This is going to be either Authorize,FinalCapture,Refund Etc.
	private String batchStatus; // Approved or authorize
	
	@JsonIgnore
	private Blob wholeResponse;
	
	
	@Id
	@GeneratedValue
	@Column(name="distribution_payoutresid",unique=true)
	public long getDistributionPayoutResponseID() {
		return distributionPayoutResponseID;
	}
	public void setDistributionPayoutResponseID(long distributionPayoutResponseID) {
		this.distributionPayoutResponseID = distributionPayoutResponseID;
	}
	
	
	public long getDistributionId() {
		return distributionId;
	}
	public void setDistributionId(long distributionId) {
		this.distributionId = distributionId;
	}
	
	
	public long getSenderBatchId() {
		return senderBatchId;
	}
	public void setSenderBatchId(long senderBatchId) {
		this.senderBatchId = senderBatchId;
	}
	
	
	public Timestamp getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Timestamp createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	public String getPayoutResponseBatchId() {
		return payoutResponseBatchId;
	}
	public void setPayoutResponseBatchId(String payoutResponseBatchId) {
		this.payoutResponseBatchId = payoutResponseBatchId;
	}
	
	
	public String getBatchStatus() {
		return batchStatus;
	}
	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}
	
	
	public Blob getWholeResponse() {
		return wholeResponse;
	}
	public void setWholeResponse(Blob wholeResponse) {
		this.wholeResponse = wholeResponse;
	}

}
