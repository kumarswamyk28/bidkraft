package com.bidkraft.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="payment")
public class UserPayment{
	
	
	private long id;
	private String txnReference;
	private long jobId;	
	private long seekerUserId;
	private long providerUserId;
	private float commissionToPS;
	private float salesTax;
	private float serviceTax;
	private float subTotal;
	private float total;
	private long paymentSystemTypeId;
	private long paymentStatusCodeId;


	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public long getPayment_id() {
		return id;
	}
	public void setPayment_id(long payment_id) {
		this.id = payment_id;
	}

	@Column(name="txn_reference")
	public String getTxnReference() {
		return txnReference;
	}
	public void setTxnReference(String txnReference) {
		this.txnReference = txnReference;
	}
	
	@Column(name="job_id")
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	
	@Column(name="seekeruser_id")
	public long getSeekerUserId() {
		return seekerUserId;
	}
	public void setSeekerUserId(long seekerUserId) {
		this.seekerUserId = seekerUserId;
	}
	
	@Column(name="provideruser_id")
	public long getProviderUserId() {
		return providerUserId;
	}
	public void setProviderUserId(long providerUserId) {
		this.providerUserId = providerUserId;
	}
	
	@Column(name="commissiontops")
	public float getCommissionToPS() {
		return commissionToPS;
	}
	public void setCommissionToPS(float commissionToPS) {
		this.commissionToPS = commissionToPS;
	}
	
	@Column(name="salestax")
	public float getSalesTax() {
		return salesTax;
	}
	public void setSalesTax(float salesTax) {
		this.salesTax = salesTax;
	}
	
	@Column(name="servicetax")
	public float getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(float serviceTax) {
		this.serviceTax = serviceTax;
	}
	
	@Column(name="subtotal")
	public float getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(float subTotal) {
		this.subTotal = subTotal;
	}
	
	@Column(name="total")
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	
	@Column(name="paymentsystemtype_id")
	public long getPaymentSystemTypeId() {
		return paymentSystemTypeId;
	}
	public void setPaymentSystemTypeId(long paymentSystemTypeId) {
		this.paymentSystemTypeId = paymentSystemTypeId;
	}
	
	@Column(name="paymentstatuscode_id")
	public long getPaymentStatusCodeId() {
		return paymentStatusCodeId;
	}
	public void setPaymentStatusCodeId(long paymentStatusCodeId) {
		this.paymentStatusCodeId = paymentStatusCodeId;
	}
	
	
}
