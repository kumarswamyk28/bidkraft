package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reviews")
public class Reviews {

	private long review_id;
	private long review_userid;
	private long user_id;
	private String description;
	private long rating;

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true)
	public long getReview_id() {
		return review_id;
	}

	public void setReview_id(long review_id) {
		this.review_id = review_id;
	}

	@Column(name = "rev_uid")
	public long getReview_userid() {
		return review_userid;
	}

	public void setReview_userid(long review_userid) {
		this.review_userid = review_userid;
	}

	@Column(name = "user_id")
	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "rating")
	public long getRating() {
		return rating;
	}

	public void setRating(long rating) {
		this.rating = rating;
	}

}
