package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "distribution")
public class Distribution {
	
	private long distributionid;
	private long jobid;
	private long sellerid;
	private double amount;
	private int currencytype;
	private int distributionstatus;
	private String transactionid;
	
	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public long getDistributionid() {
		return distributionid;
	}
	public void setDistributionid(long distributionid) {
		this.distributionid = distributionid;
	}
	
	@Column(name="job_id")
	public long getJobid() {
		return jobid;
	}
	public void setJobid(long jobid) {
		this.jobid = jobid;
	}
	
	@Column(name="seller_id")
	public long getSellerid() {
		return sellerid;
	}
	public void setSellerid(long sellerid) {
		this.sellerid = sellerid;
	}
	
	@Column(name="amount")
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Column(name="currency_type")
	public int getCurrencytype() {
		return currencytype;
	}
	public void setCurrencytype(int currencytype) {
		this.currencytype = currencytype;
	}
	
	@Column(name="distribution_status")
	public int getDistributionstatus() {
		return distributionstatus;
	}
	public void setDistributionstatus(int distributionstatus) {
		this.distributionstatus = distributionstatus;
	}
	@Column(name="transaction_id")
	public String getTransactionid() {
		return transactionid;
	}
	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}


}
