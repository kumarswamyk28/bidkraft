package com.bidkraft.entities;

public class DummyFuturePaymentEntity {	
	private long userId;
	private String clientMetaDataId;
	private Float amount;
	
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getClientMetaDataId() {
		return clientMetaDataId;
	}
	public void setClientMetaDataId(String clientMetaDataId) {
		this.clientMetaDataId = clientMetaDataId;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	
}
