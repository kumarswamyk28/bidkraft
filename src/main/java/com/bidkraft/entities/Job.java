package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="job")
public class Job {

	private long job_id;
	private long request_id;
	private long bid_id;
	private int status;


	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public long getJob_id() {
		return job_id;
	}
	public void setJob_id(long job_id) {
		this.job_id = job_id;
	}
	

	@Column(name="request_id")
	public void setRequest_id(long request_id) {
		this.request_id = request_id;
	}
	public long getRequest_id() {
		return request_id;
	}
	
	
	@Column(name="bid_id")
	public void setBid_id(long bidId) {
		this.bid_id = bidId;
	}
	public long getBid_id() {
		return bid_id;
	}
	
	@Column(name="status")
	public void setStatus(int status) {
		this.status = status;
	}
	public int getStatus() {
		return status;
	}
	
	
}
