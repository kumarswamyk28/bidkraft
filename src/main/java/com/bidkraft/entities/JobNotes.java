package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="job")
public class JobNotes {
	
	private long jobnotesid;
	private long parentid;
	private long userid;
	private String note;
	
	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public long getJobnotesid() {
		return jobnotesid;
	}
	public void setJobnotesid(long jobnotesid) {
		this.jobnotesid = jobnotesid;
	}
	public long getParentid() {
		return parentid;
	}
	public void setParentid(long parentid) {
		this.parentid = parentid;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}


}
