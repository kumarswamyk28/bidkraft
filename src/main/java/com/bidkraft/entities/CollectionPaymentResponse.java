package com.bidkraft.entities;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;


@Entity
@Table(name = "collection_paymentresponse")
public class CollectionPaymentResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private long collectionPaymentResponseID;
	private long collectionId; //this is the foriegn key with the Collection class ID
	private String responseId; // This is the ID that comes with the response of the paypal calls
	private Timestamp createdDateTime;
	private String intent; //This is going to be either Authorize,FinalCapture,Refund Etc.
	private String state; // Approved or authorize
	private String parentPaymentId; 
	private String authorizationId; // this can be null some times, usually this is the authorization ID under authorization object of the response, the future capture call are going to be on this.
	
	@JsonIgnore
	private Blob wholeResponse;
	//Collection collection;
	

	@Id
	@GeneratedValue
	@Column(name="collection_payresid",unique=true)
	public long getCollectionPaymentResponseID() {
		return collectionPaymentResponseID;
	}
	public void setCollectionPaymentResponseID(long collectionPaymentResponseID) {
		this.collectionPaymentResponseID = collectionPaymentResponseID;
	}
	
	
	
	@Column(name="collection_id")
	public long getCollectionId() {
		return collectionId;
	}
	public void setCollectionId(long collectionId) {
		this.collectionId = collectionId;
	}
	
	
	@Column(name="response_id")
	public String getResponseId() {
		return responseId;
	}
	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
	
	@Column(name="createddatetime")
	public Timestamp getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Timestamp createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Column(name="intent")
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	
	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="parent_payment_id")
	public String getParentPaymentId() {
		return parentPaymentId;
	}
	public void setParentPaymentId(String parentPaymentId) {
		this.parentPaymentId = parentPaymentId;
	}
	
	@Column(name="authorization_id")
	public String getAuthorizationId() {
		return authorizationId;
	}
	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}
	
	@Column(name="wholeresponse")
	public Blob getWholeResponse() {
		return wholeResponse;
	}
	public void setWholeResponse(Blob blob) {
		this.wholeResponse = blob;
	}

	
/*	@ManyToOne
	@JoinColumn(name="collection_id")
	public Collection getCollection() {
		return collection;
	}
	public void setCollection(Collection collection) {
		this.collection = collection;
	}*/

}
