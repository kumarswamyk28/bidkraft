package com.bidkraft.entities;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="futuretranxauthorization")
public class FutureTranxAuthorization {

	private long futureTranxAuth_id;
	private String authCode;
	private long user_id;
	private String accessToken;
	private String refreshToken;
	private long expiresIn;
	private Timestamp createdDateTime;
	private Timestamp lastRefreshedDateTime;
	private String clientMetaData_id;

	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public long getFutureTranxAuth_id() {
		return futureTranxAuth_id;
	}
	public void setFutureTranxAuth_id(long futureTranxAuth_id) {
		this.futureTranxAuth_id = futureTranxAuth_id;
	}

	
	@Column(name="accesstoken")
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	
	@Column(name="user_id")
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
 
	
	@Column(name="refreshtoken")
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	
	
	@Column(name="expiresin")
	public long getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}
	
	
	@Column(name="createddatetime")
	public Timestamp getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Timestamp createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	
	@Column(name="lastrefresheddatetime")
	public Timestamp getLastRefreshedDateTime() {
		return lastRefreshedDateTime;
	}
	public void setLastRefreshedDateTime(Timestamp lastRefreshedDateTime) {
		this.lastRefreshedDateTime = lastRefreshedDateTime;
	}
	
	
	@Column(name="clientmetadata_id")
	public String getClientMetaData_id() {
		return clientMetaData_id;
	}
	public void setClientMetaData_id(String clientMetaData_id) {
		this.clientMetaData_id = clientMetaData_id;
	}
	
	@Column(name="authkey")
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

}
