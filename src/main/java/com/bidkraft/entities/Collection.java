package com.bidkraft.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "collection")
public class Collection implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private long collectionid;
	private long jobid;
	private long buyerid;
	private double amount;
	private int currencytype;
	private int collectionstatus;
	private String clientMetaDataId;
	
	
/*	List<CollectionPaymentResponse> collectionResList = new ArrayList<CollectionPaymentResponse>();
*/	
	
	@Id
	@GeneratedValue
	@Column(name="collection_id",unique=true)
	public long getCollectionid() {
		return collectionid;
	}

	public void setCollectionid(long collectionid) {
		this.collectionid = collectionid;
	}
	
	@Column(name="job_id")
	public long getJobid() {
		return jobid;
	}
	public void setJobid(long jobid) {
		this.jobid = jobid;
	}
	
	@Column(name="buyer_id")
	public long getBuyerid() {
		return buyerid;
	}
	public void setBuyerid(long buyerid) {
		this.buyerid = buyerid;
	}
	
	@Column(name="amount")
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Column(name="currency_type")
	public int getCurrencytype() {
		return currencytype;
	}
	public void setCurrencytype(int currencytype) {
		this.currencytype = currencytype;
	}
	
	@Column(name="collection_status")
	public int getCollectionstatus() {
		return collectionstatus;
	}
	public void setCollectionstatus(int collectionstatus) {
		this.collectionstatus = collectionstatus;
	}
	
	@Column(name="clientmetadata_id")
	public String getClientMetaDataId() {
		return clientMetaDataId;
	}
	public void setClientMetaDataId(String clientMetaDataId) {
		this.clientMetaDataId = clientMetaDataId;
	}
	
/*	@OneToMany(mappedBy="collection",cascade=CascadeType.ALL)
	public List<CollectionPaymentResponse> getCollectionResList() {
		return collectionResList;
	}
	public void setCollectionResList(List<CollectionPaymentResponse> collectionResList) {
		this.collectionResList = collectionResList;
	}*/
	
	
}
