package com.bidkraft.entities;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;

@Entity
@Table(name="user")
public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("userid")
	private long userid;
	
	@JsonProperty("email")
	private String email;
	
	@JsonIgnore
	private String password;
	
	@JsonProperty("lat")
	private double latitude;
	
	@JsonProperty("lon")
	private double longitude;
	
	@JsonProperty("firstname")
	private String firstName;
	

	@JsonProperty("lastname")
	private String lastName;
	
	@JsonProperty("phone")
	private String phone;
	
	@JsonProperty("bgcstatus")
	private String bgcstatus;
	
	@JsonProperty("rating")
	private double rating;
	
	@JsonProperty("image")
    private byte[] image;
	
	@JsonProperty("iostoken")
	private String iOSToken;
	
	@JsonProperty("andriodtoken")
	private String andriodToken;
	
	@JsonProperty("ispaypalauth")
	private String isPayPalAuth;
    
	
	@JsonProperty("paypalregisteredemail")
	private String paypalRegisteredEmail;
	
	
	@JsonProperty("paypaluserurl")
	private String paypalUserURL;
	
	@JsonIgnore
	private Blob paypaIdentityResponse;
	
	
	@Id
	@GeneratedValue
	@Column(name="userid",unique=true)
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="lat")
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	@Column(name="lon")
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	@Column(name="bgc_status")
	public String getBgcstatus() {
		return bgcstatus;
	}
	public void setBgcstatus(String bgcstatus) {
		this.bgcstatus = bgcstatus;
	}
	
	@Column(name="rating")
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	
	@Column(name="ios_token")
	public String getiOSToken() {
		return iOSToken;
	}
	
	
	@Column(name="ispaypalauth")
	public String getIsPayPalAuth() {
		return isPayPalAuth;
	}

	public void setIsPayPalAuth(String isPayPalAuth) {
		this.isPayPalAuth = isPayPalAuth;
	}

	public void setiOSToken(String iOSToken) {
		this.iOSToken = iOSToken;
	}
	
	@Column(name="andriod_token")
	public String getAndriodToken() {
		return andriodToken;
	}
	
	public void setAndriodToken(String andriodToken) {
		this.andriodToken = andriodToken;
	}
	
	@Lob
    @Column(name="profile_image", nullable=true, columnDefinition="mediumblob")
	public byte[] getImage() {
        return image;
    }
 
    public void setImage(byte[] image) {
        this.image = image;
    }
    
	@Column(name="paypalregisteredemail")
    public String getPaypalRegisteredEmail() {
		return paypalRegisteredEmail;
	}

	public void setPaypalRegisteredEmail(String paypalRegisteredEmail) {
		this.paypalRegisteredEmail = paypalRegisteredEmail;
	}
	
	
	@Column(name="paypaluserurl")
    public String getPaypalUserURL() {
		return paypalUserURL;
	}

	public void setPaypalUserURL(String paypalUserURL) {
		this.paypalUserURL = paypalUserURL;
	}

	@Column(name="firstname")
	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="lastname")
	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name="phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	@Column(name="paypalidentityresponse")
	public Blob getPaypaIdentityResponse() {
		return paypaIdentityResponse;
	}

	public void setPaypaIdentityResponse(Blob paypaIdentityResponse) {
		this.paypaIdentityResponse = paypaIdentityResponse;
	}

    
}
