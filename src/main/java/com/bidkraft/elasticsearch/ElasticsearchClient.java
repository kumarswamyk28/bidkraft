package com.bidkraft.elasticsearch;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.stereotype.Component;

@Component
public class ElasticsearchClient {

	private static Client esClient;

	public Client getClient() throws UnknownHostException {
		if (esClient != null) {
			return esClient;
		} else {

			Settings settings = Settings.settingsBuilder().put("cluster.name", "elasticsearch").build();
			Client client = TransportClient.builder().settings(settings).build()
					.addTransportAddress(new InetSocketTransportAddress(
							InetAddress.getByName("ec2-54-166-140-220.compute-1.amazonaws.com"), 9300));
			//ec2-52-37-18-82.us-west-2.compute.amazonaws.com
			ElasticsearchClient.esClient = client;
			return client;
		}

	}

}
