package com.bidkraft.elasticsearch;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import com.bidkraft.entities.Request;
import com.bidkraft.exception.KraftException;

public class FinderAPI {

	private static ObjectMapper mapper;

	public ObjectMapper getObjectMapper() {
		if (mapper != null) {
			return mapper;
		} else {
			ObjectMapper mapper = new ObjectMapper().setVisibility(JsonMethod.FIELD, Visibility.ANY);
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
			mapper.setDateFormat(df);
			FinderAPI.mapper = mapper;
			return mapper;
		}

	}

	public Request getRequest(String id) throws KraftException {
		Request result = new Request();
		try {
			Client esClient = new ElasticsearchClient().getClient();
			GetResponse response = esClient.prepareGet("servicerequest", "request", id).get();
			if (!response.isSourceEmpty()) {
				result = getObjectMapper().readValue(response.getSourceAsString(), Request.class);
			} else {
				throw new Exception("Couldn't it the request. Please check the request Id");
			}
			return result;
		} catch (Exception e) {
			throw new KraftException("FinderAPI getRequest" + e.getMessage());
		}
	}

	public <T> List<T> getResult(QueryBuilder query, String type, int from, int size, Class<T> classz) throws KraftException {
		List<T> result = new ArrayList<T>();
		try {
			Client esClient = new ElasticsearchClient().getClient();
			SearchResponse response = esClient.prepareSearch("servicerequest").setTypes(type)
					.setQuery(QueryBuilders.constantScoreQuery(query)).setFrom(from).setSize(size).get();

			if (response.getHits().totalHits() > 0) {
				for (SearchHit hit : response.getHits()) {
					try {
						result.add(getObjectMapper().readValue(hit.getSourceAsString(), classz));
					} catch (JsonParseException e) {
						throw new KraftException("Json Parse Exception " + e.getMessage());
					} catch (JsonMappingException e) {
						throw new KraftException("Json Mapping Exception " + e.getMessage());
					} catch (IOException e) {
						throw new KraftException("IOException " + e.getMessage());
					}
				}
			}
			return result;
		} catch (UnknownHostException e) {
			throw new KraftException("Unknown Host Exception " + e.getMessage());
		} catch (Exception e) {
			throw new KraftException(e.getMessage());
		}
	}

}
