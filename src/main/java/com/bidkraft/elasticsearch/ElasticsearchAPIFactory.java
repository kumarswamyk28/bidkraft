package com.bidkraft.elasticsearch;

public class ElasticsearchAPIFactory {
	
	private static IndexAPI indexAPI = null;
	private static FinderAPI finderAPI = null;
	
	
	public static IndexAPI getIndexAPI() {
		if(indexAPI == null) {
			indexAPI = new IndexAPI();
		}
		return indexAPI;
	}
	
	public static FinderAPI getFinderAPI() {
		if(finderAPI == null){
			finderAPI = new FinderAPI();
		}
		return finderAPI;
	}

}
