package com.bidkraft.constants;

public class ServiceKeys {
	
	public static final String LOGIN="LOGIN";
	public static final String REGISTRATION="REGISTRATION";
	public static final String VERIFICATION="VERIFICATION";
	public static final String CREQ="CREQ";//REQUEST 
	public static final String CBID="CBID";//CREATE Bid 
	public static final String CREV="CREV";//CREATE USER REVIEW
	
	public static final String ABID="ABID";//ACCEPT BID
	public static final String UBID="UBID";//UPDATE BID
	
	public static final String GETAVAILABLEREQUESTS="GETAVAILABLEREQ"; 
	public static final String GETUSERREQUESTS="GETUSERREQ"; 
	public static final String GETUSERBIDS="GETUSERBID";
	public static final String GETUSERJOBS="GETUSERJOBS";
	public static final String GETREQUESTBIDS="GETREQUESTBID";
	public static final String GETUSERDETAILS="GETUSERDETAILS";
	public static final String GETUSERREVIEWS="GETUSERREVIEWS";
	public static final String GETREQUESTINFO="GETREQUESTINFO";
	public static final String BGC_INTIAL="INITIALIZED";
	
	public static final String GETMYREQUESTS="GETMYREQUESTS";
	public static final String GETMYBIDS="GETMYBIDS";
	
	public static final String POSTPAY="POSTPAY";
	public static final String FINISHJOB="FINISHJOB";
	
	public static final String UPDATETOCKEN="UPDATETOCKEN";
	public static final String CREATEBIDNOTIFICATION="CREATEBIDNOTIFICATION";
	
	//Payments Related & the services that need change because of the Business model change
	public static final String GETALLUSERPAYMENTS ="GETALLUSERPAYMENTS";
	public static final String GETPAYPALMERCHANTDETAILS= "GETPAYPALMERCHANTDETAILS";
	public static final String GETUSERPAYMENTPRICELINES= "GETUSERPAYMENTPRICELINES";
	public static final String GETPAYPALPAYMENTHISTORY = "GETPAYPALPAYMENTHISTORY";
	public static final String GETPAYPALPAYOUTBATCHDET = "GETPAYPALPAYOUTBATCHDET";
	public static final String INITIATEPAYPALPAYMENT = "INITIATEPAYPALPAYMENT";
	public static final String COMPLETEPAYPALPAYMENT = "COMPLETEPAYPALPAYMENT";
	public static final String SINGLEPAYPALPAYOUT = "SINGLEPAYPALPAYOUT";
	public static final String STOREFUTURETRANXAUTHKEY = "STOREFUTURETRANXAUTHKEY";
	public static final String PAYBYFUTUREPAYMENT = "PAYBYFUTUREPAYMENT";
	
	public static final String CREATEBIDVERSION1="CREATEBIDVERSION1";//CREATE Bid Version 1
	public static final String INITIATEPAYPALFUTUREPAYMENT = "INITIATEPAYPALFUTUREPAYMENT"; // this one to initiate paypal future payment

	public static final String GETUSERINFOFROMPAYPAL = "GETUSERINFOFROMPAYPAL"; // this one is for login through Paypal 

	public static final String TESTDUMMY = "TESTDUMMY";
	
	
}























