package com.bidkraft.responses;

import com.bidkraft.entities.Bid;
import com.bidkraft.entities.User;

public class UserBid extends Bid {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	
	public UserBid(Bid bid, User user) {
	    super(bid);
	    this.user = user;
	}
	
	public User getUser() {
		return this.user;
	}
}
