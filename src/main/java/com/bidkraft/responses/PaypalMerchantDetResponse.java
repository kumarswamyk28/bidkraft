package com.bidkraft.responses;

public class PaypalMerchantDetResponse {

	private String metchantId;
	private String merchantSecret;
	
	public PaypalMerchantDetResponse(String metchantId, String merchantSecret) {
		this.metchantId = metchantId;
		this.merchantSecret = merchantSecret;
	}
	
	
	public String getMetchantId() {
		return metchantId;
	}

	public void setMetchantId(String metchantId) {
		this.metchantId = metchantId;
	}

	public String getMerchantSecret() {
		return merchantSecret;
	}

	public void setMerchantSecret(String merchantSecret) {
		this.merchantSecret = merchantSecret;
	}

	
	
	
}
