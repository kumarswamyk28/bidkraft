package com.bidkraft.responses;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.bidkraft.entities.Job;
import com.bidkraft.entities.Request;

public class UserRequestResponse {

	@JsonIgnore
	public long requestId;
	public Request request;
	//public BidsResponse bids;
	
	public Request getRequest() {
		return request;
	}
	public List<UserBid> bids;
	public Job job;
	
	public UserRequestResponse(long requestId, Request request, List<UserBid> bids) {
		this.requestId = requestId;
		this.request = request;
		this.bids = bids;
	}
	
	public UserRequestResponse(long requestId, Request request, List<UserBid> bids, Job job) {
		this.requestId = requestId;
		this.request = request;
		this.bids = bids;
		this.job = job;
	}
	
	
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public void setBids(List<UserBid > bids) {
		this.bids = bids;
	}
	
	
}
