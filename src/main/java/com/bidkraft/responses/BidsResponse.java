package com.bidkraft.responses;

import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class BidsResponse {

	@JsonIgnore
	public long requestId;
	
	@JsonIgnore
	@JsonProperty("totalbids")
	public int totalBids;
	public List<UserBid> bids;
	
	public BidsResponse() {
		
	}
	
	public BidsResponse(Long requestId, int totalBids, List<UserBid> bids) {
		this.requestId = requestId;
		this.totalBids = totalBids;
		this.bids = bids;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public void setTotalBids(int totalBids) {
		this.totalBids = totalBids;
	}

	public void setBids(List<UserBid> bids) {
		this.bids = bids;
	}
	
	
	
}
